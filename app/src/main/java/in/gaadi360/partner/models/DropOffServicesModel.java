package in.gaadi360.partner.models;

import java.util.List;

public class DropOffServicesModel
{
    public String serviceId;
    public String serviceName;
    public String bookingId;
    public String createdTime;
    public String modifiedTime;
    public String cost;
    public String type;
    public List<ServiceDetailModel> serviceDetailModels;

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ServiceDetailModel> getServiceDetailModels() {
        return serviceDetailModels;
    }

    public void setServiceDetailModels(List<ServiceDetailModel> serviceDetailModels) {
        this.serviceDetailModels = serviceDetailModels;
    }
}
