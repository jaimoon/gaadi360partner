package in.gaadi360.partner.models;

public class ServicesModel {

    public static final int HEADER_TYPE = 0;
    public static final int CHILD_TYPE = 1;

    public String serviceId;
    public String serviceName;
    public String createdTime;
    public String modifiedTime;
    public String bookingId;
    public String cost;
    public String type;
    public String perform;
    public String serviceItem;
    public String status;
    public int value;

    public ServicesModel(String serviceId, String serviceName, String createdTime, String modifiedTime, String bookingId, String cost, String type,
                         String perform, String serviceItem, String status, int value) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.createdTime = createdTime;
        this.modifiedTime = modifiedTime;
        this.bookingId = bookingId;
        this.cost = cost;
        this.type = type;
        this.perform = perform;
        this.serviceItem = serviceItem;
        this.status = status;
        this.value = value;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPerform() {
        return perform;
    }

    public void setPerform(String perform) {
        this.perform = perform;
    }

    public String getServiceItem() {
        return serviceItem;
    }

    public void setServiceItem(String serviceItem) {
        this.serviceItem = serviceItem;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }



}
