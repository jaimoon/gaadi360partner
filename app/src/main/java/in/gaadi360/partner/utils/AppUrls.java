package in.gaadi360.partner.utils;

public class AppUrls {

    public static String BASE_URL = "https://admin.gaadi360.com/gaadi2/api/";
    //public static String BASE_URL = "http://test.gaadi360.com/gaadi/api/";
    //public static String BASE_URL = "http://192.168.32.97:8080/gaadi/api/";

    public static String IMAGE_URL = BASE_URL + "media/file/display/";
    public static String SERVICETYPE = "service/type";

    public static String VERSION_UPDATE = "build/info/1/2";
    public static String PROFILE = "secure/profile/get";
    public static String EDIT_PROFILE = "secure/profile/update";
    public static String EDIT_PROFILE_PIC = "media/file/upload";
    public static String HELP_CENTER = "secure/help-center/get";
    public static String LOGIN = "login/generate/otp?mobile=";
    public static String VERIFYOTP = "login/via/otp?mobile=";
    public static String FCM = "secure/user/device";
    public static String HISTORY = "secure/V2/service_center/bookings?status=";
    public static String HISTORY_DETAIL = "secure/booking/";
    public static String NOTIFICATIONS = "secure/notifications";
    public static String READ_UNREAD = "secure/notification/";
    public static String CLEAR_NOTIFICATIONS = "secure/notification/all/clear";
    public static String NOTIFICATION_COUNT = "secure/notifications/unread-count";
    public static String ASSIGN_PICKUP = "secure/service_center/pickup_persons";
    public static String ASSIGNED_PICKUP = "/assign/pickup_person?pickupUserId=";
    public static String PICKUP_OTP = "secure/generate_otp?bookingId=";
    public static String PICKUP_VERIFY_OTP = "secure/V2/verify/otp?otp=";
    public static String SEND_PICKUP = "secure/booking/pickup";
    public static String SEND_MECHANIC = "secure/service_center/mechanics";
    public static String ASSIGN_MECHANIC = "/assign/mechanic?mechanicUserId=";
    public static String ADDON_SERVICES = "secure/service-center/";
    public static String SEND_ADDON = "secure/booking/services/add";
    public static String SERVICE_COMPLETED = "secure/v2/booking/service_completed";
    public static String SEND_BIKE_IMAGE = "media/file/upload";
    public static String DROP_OFF_INSPECTIONS = "V2/inspections";
    public static String DROP_OFF = "secure/booking/";
    public static String DELIVERED = "secure/booking/";
    public static String CALL_OTP = "V2/login/resend/otp?mobile=";

}

