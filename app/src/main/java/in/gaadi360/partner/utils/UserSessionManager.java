package in.gaadi360.partner.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

import in.gaadi360.partner.MainActivity;
import in.gaadi360.partner.activities.LoginActivity;

public class UserSessionManager {

    private SharedPreferences sharedPreferences;
    private Editor editor;
    private Context _context;
    private static final String PREFER_NAME = "Gaadi360Partner";
    private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    public static final String INTRO_SLIDE = "intorslide";
    public static final String ISFIRTTIMEAPP = "isfirsttime";
    public static final String DEVICE_ID = "device_id";
    public static final String KEY_ACCSES = "access_key";
    public static final Integer USER_ID = 100;
    public static final String FIRST_NAME = "firstName";
    public static final String USER_NAME = "user_name";
    public static final String USER_MOBILE = "user_mobile";
    public static final String USER_EMAIL = "user_email";
    public static final String MEDIA_SECRET = "mediaSecret";

    @SuppressLint("CommitPrefEdits")
    public UserSessionManager(Context context) {
        this._context = context;
        int PRIVATE_MODE = 0;

        sharedPreferences = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createDeviceId(String deviceId) {
        editor.putString(DEVICE_ID, deviceId);
        editor.apply();
    }

    public boolean checkLogin() {

        if (this.isUserLoggedIn()) {

            Intent i = new Intent(_context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
            return true;
        }
        return false;
    }

    public void createUserLoginSession(String firstName, String username, String email, String mobile, String accessToken, String mediaSecret) {
        editor.putBoolean(IS_USER_LOGIN, true);
        //editor.putInt(USER_ID, userId);
        editor.putString(FIRST_NAME, firstName);
        editor.putString(USER_NAME, username);
        editor.putString(USER_EMAIL, email);
        editor.putString(USER_MOBILE, mobile);
        editor.putString(KEY_ACCSES, accessToken);
        editor.putString(MEDIA_SECRET, mediaSecret);
        editor.apply();
    }

    public HashMap<String, String> getUserDetails() {

        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ACCSES, sharedPreferences.getString(KEY_ACCSES, null));
        //user.put(USER_ID, sharedPreferences.getInt(USER_ID, null));
        user.put(USER_NAME, sharedPreferences.getString(USER_NAME, null));
        user.put(USER_MOBILE, sharedPreferences.getString(USER_MOBILE, null));
        user.put(USER_EMAIL, sharedPreferences.getString(USER_EMAIL, null));
        user.put(DEVICE_ID, sharedPreferences.getString(DEVICE_ID, null));
        return user;
    }

    public boolean isUserLoggedIn() {
        return !sharedPreferences.getBoolean(IS_USER_LOGIN, false);
    }

    public void logoutUser(){
        editor.clear();
        editor.apply();
        Intent i = new Intent(_context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

}