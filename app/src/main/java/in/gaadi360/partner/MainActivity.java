package in.gaadi360.partner;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.google.android.material.snackbar.Snackbar;
import com.nex3z.notificationbadge.NotificationBadge;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import in.gaadi360.partner.activities.DeliveredActivity;
import in.gaadi360.partner.activities.HistoryActivity;
import in.gaadi360.partner.activities.NotificationActivity;
import in.gaadi360.partner.activities.SettingActivity;
import in.gaadi360.partner.adapters.NewHistoryAdapter;
import in.gaadi360.partner.models.NewHistoryModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Boolean exit = false;
    private boolean checkInternet;
    ImageView share_img, settings_img, notification_img;
    Typeface regular, bold;
    UserSessionManager userSessionManager;

    ImageView no_history_img;
    TextView toolbar_title, no_history_txt;
    RecyclerView history_recyclerview;
    SearchView history_search;
    String accessToken;
    NotificationBadge badge;
    AlertDialog versionDialog;
    int version = 0;

    /*History*/
    NewHistoryAdapter historyAdapter;
    ArrayList<NewHistoryModel> historyModels = new ArrayList<NewHistoryModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        setupBottomBar();

        checkVersionUpdate();

        share_img = findViewById(R.id.share_img);
        share_img.setOnClickListener(this);

        settings_img = findViewById(R.id.settings_img);
        settings_img.setOnClickListener(this);

        badge = findViewById(R.id.badge);
        notification_img = findViewById(R.id.notification_img);
        notification_img.setOnClickListener(this);

        no_history_img = findViewById(R.id.no_history_img);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        no_history_txt = findViewById(R.id.no_history_txt);
        no_history_txt.setTypeface(bold);

        history_recyclerview = findViewById(R.id.history_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        history_recyclerview.setLayoutManager(layoutManager);
        historyAdapter = new NewHistoryAdapter(historyModels, MainActivity.this, R.layout.row_new_history);

        getNotificationCount();

        getHistory();

        history_search = findViewById(R.id.history_search);
        EditText searchEditText = history_search.findViewById(androidx.appcompat.R.id.search_src_text);
        history_search.setOnClickListener(v -> history_search.setIconified(false));
        history_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                historyAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getNotificationCount() {

        String url = AppUrls.BASE_URL + AppUrls.NOTIFICATION_COUNT;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);


                    if (jsonObject.length() != 0) {

                        badge.setVisibility(View.VISIBLE);

                        String notificationUnreadCount = jsonObject.optString("notificationUnreadCount");
                        badge.setNumber(Integer.parseInt(notificationUnreadCount));

                    } else {
                        badge.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    private void getHistory() {

        //String url = AppUrls.BASE_URL + AppUrls.HISTORY + "1";
        String url = AppUrls.BASE_URL + AppUrls.HISTORY + "4";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                historyModels.clear();

                try {

                    JSONArray jsonArray = new JSONArray(response);


                    if (jsonArray.length() != 0) {

                        history_search.setVisibility(View.VISIBLE);
                        no_history_img.setVisibility(View.GONE);
                        no_history_txt.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            NewHistoryModel rm = new NewHistoryModel();
                            rm.setBookingId(jsonObject1.optString("bookingId"));
                            rm.setBrandId(jsonObject1.optString("brandId"));
                            rm.setBrandName(jsonObject1.optString("brandName"));
                            rm.setModelId(jsonObject1.optString("modelId"));
                            rm.setModelName(jsonObject1.optString("modelName"));
                            rm.setRegistrationNumber(jsonObject1.optString("registrationNumber"));
                            rm.setBookingDate(jsonObject1.optString("bookingDate"));
                            rm.setBookingTime(jsonObject1.optString("bookingTime"));
                            rm.setFinalPrice(jsonObject1.optString("finalPrice"));
                            rm.setPickupAddress(jsonObject1.optString("pickupAddress"));
                            rm.setStatus(jsonObject1.optString("status"));
                            rm.setServiceCenterName(jsonObject1.optString("serviceCenterName"));
                            rm.setPaymentStatus(jsonObject1.optString("paymentStatus"));
                            rm.setPromocodeAmount(jsonObject1.optString("promocodeAmount"));
                            rm.setMovingCondition(jsonObject1.optBoolean("movingCondition"));

                            String services = "";
                            if (jsonObject1.has("services")) {
                                JSONArray servicesArray = jsonObject1.getJSONArray("services");

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    services = IntStream.range(0, servicesArray.length()).mapToObj(j -> {
                                        try {
                                            return servicesArray.getJSONObject(j).getString("serviceName");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            return "";
                                        }
                                    }).collect(Collectors.joining("\n"));
                                }

                                rm.setServices(services);
                            }

                            historyModels.add(rm);
                        }

                        history_recyclerview.setAdapter(historyAdapter);
                        historyAdapter.notifyDataSetChanged();
                    } else {
                        history_search.setVisibility(View.GONE);
                        no_history_txt.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {

        if (v == share_img) {

            if (checkInternet) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Gaadi360");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "A two-wheeler service that's sure to give you more SMILEAGE.\n" +
                        "Enjoy unparalleled service with Gaadi360.  \n" +
                        "Download at app.gaadi360.com");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == settings_img) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, SettingActivity.class);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

        if (v == notification_img) {

            if (checkInternet) {
                Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(intent);
                finish();

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

        }

    }

    private void setupBottomBar() {
        BottomNavigationBar bottomNavigationBar = findViewById(R.id.bottom_bar);

        BottomBarItem home = new BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.newHistory);
        BottomBarItem history = new BottomBarItem(R.drawable.ic_diploma, R.string.inProcessHistory);
        BottomBarItem settings = new BottomBarItem(R.drawable.ic_diploma, R.string.deliveredHistory);

        bottomNavigationBar
                .addTab(home)
                .addTab(history)
                .addTab(settings);

        bottomNavigationBar.selectTab(0, true);

        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                showContent(position);
            }
        });
    }

    void showContent(int position) {

        if (position == 1) {
            Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
            startActivity(intent);
        }
        if (position == 2) {
            Intent intent = new Intent(MainActivity.this, DeliveredActivity.class);
            startActivity(intent);
        }
    }

    private void checkVersionUpdate() {

        try {
            PackageInfo pInfo = MainActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionCode;
            Log.d("VERSION", "" + version);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String url = AppUrls.BASE_URL + AppUrls.VERSION_UPDATE;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String id = jsonObject.optString("id");
                    String platformId = jsonObject.optString("platformId");
                    int currentVersionCode = jsonObject.optInt("currentVersionCode");
                    int mandatoryUpdateVersionCode = jsonObject.optInt("mandatoryUpdateVersionCode");
                    String versionName = jsonObject.optString("versionName");
                    String mandatory = jsonObject.optString("mandatory");
                    String status = jsonObject.optString("status");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");

                    if (mandatoryUpdateVersionCode > version) {
                        versionUpdateAlert();
                    } else {
                        if (currentVersionCode > version) {
                            optionalVersionUpdateAlert();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        GlobalCalls.showToast("Connection Not Found..!", MainActivity.this);

                    } else if (error instanceof AuthFailureError) {

                        GlobalCalls.showToast("User Not Found..!", MainActivity.this);

                    } else if (error instanceof ServerError) {

                        GlobalCalls.showToast("Server Not Found..!", MainActivity.this);


                    } else if (error instanceof NetworkError) {

                        GlobalCalls.showToast("Network Not Found..!", MainActivity.this);

                    } else if (error instanceof ParseError) {

                        GlobalCalls.showToast("Please Try Later..!", MainActivity.this);

                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(MainActivity.this);
        requestQueue.add(stringRequest);

    }

    public void versionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout);
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    public void optionalVersionUpdateAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.version_update_dialog, null);

        TextView title_txt = dialog_layout.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        TextView msg_txt = dialog_layout.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        Button update_btn = dialog_layout.findViewById(R.id.update_btn);
        update_btn.setTypeface(bold);

        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkInternet) {
                    final String appPackageName = getPackageName();
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        });

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });
        versionDialog = builder.create();
        versionDialog.setCancelable(false);
        versionDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);

        } else {

            GlobalCalls.showToast("Press Back again to Exit.!", MainActivity.this);

            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }
}
