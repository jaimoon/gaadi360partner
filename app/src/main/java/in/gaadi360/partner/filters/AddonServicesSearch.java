package in.gaadi360.partner.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.gaadi360.partner.adapters.AddonServicesAdapter;
import in.gaadi360.partner.models.AddonServicesModel;

public class AddonServicesSearch extends Filter {

    AddonServicesAdapter adapter;
    ArrayList<AddonServicesModel> filterList;

    public AddonServicesSearch(ArrayList<AddonServicesModel> filterList, AddonServicesAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<AddonServicesModel> filteredPlayers = new ArrayList<AddonServicesModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getServiceId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getServiceName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getCost().toUpperCase().contains(constraint) ||
                        filterList.get(i).getCreatedTime().toUpperCase().contains(constraint) ||
                        filterList.get(i).getModifiedTime().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.addonServicesModels = (ArrayList<AddonServicesModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
