package in.gaadi360.partner.filters;

import android.widget.Filter;

import java.util.ArrayList;

import in.gaadi360.partner.adapters.NewHistoryAdapter;
import in.gaadi360.partner.models.NewHistoryModel;

public class NewHistorySearch extends Filter {

    NewHistoryAdapter adapter;
    ArrayList<NewHistoryModel> filterList;

    public NewHistorySearch(ArrayList<NewHistoryModel> filterList, NewHistoryAdapter adapter) {
        this.adapter = adapter;
        this.filterList = filterList;
    }

    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results = new FilterResults();
        if (constraint != null && constraint.length() > 0) {
            constraint = constraint.toString().toUpperCase();

            ArrayList<NewHistoryModel> filteredPlayers = new ArrayList<NewHistoryModel>();

            for (int i = 0; i < filterList.size(); i++) {
                if (filterList.get(i).getServiceCenterName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingId().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBrandName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getModelName().toUpperCase().contains(constraint) ||
                        filterList.get(i).getServices().toUpperCase().contains(constraint) ||
                        filterList.get(i).getFinalPrice().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingDate().toUpperCase().contains(constraint) ||
                        filterList.get(i).getBookingTime().toUpperCase().contains(constraint)) {
                    filteredPlayers.add(filterList.get(i));
                }
            }
            results.count = filteredPlayers.size();
            results.values = filteredPlayers;
        } else {
            results.count = filterList.size();
            results.values = filterList;
        }
        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.historyModels = (ArrayList<NewHistoryModel>) results.values;
        adapter.notifyDataSetChanged();
    }
}
