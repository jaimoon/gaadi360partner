package in.gaadi360.partner.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.DeliveredActivity;
import in.gaadi360.partner.activities.BookingDetailActivity;
import in.gaadi360.partner.filters.DeliveredHistorySearch;
import in.gaadi360.partner.interfaces.HistoryItemClickListener;
import in.gaadi360.partner.models.DeliveredHistoryModel;

public class DeliveredHistoryAdapter extends RecyclerView.Adapter<DeliveredHistoryAdapter.DeliveredHistoryHolder> implements Filterable {

    public ArrayList<DeliveredHistoryModel> historyModels, filterList;
    DeliveredActivity context;
    DeliveredHistorySearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public DeliveredHistoryAdapter(ArrayList<DeliveredHistoryModel> historyModels, DeliveredActivity context, int resource) {
        this.historyModels = historyModels;
        this.filterList = historyModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public DeliveredHistoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_delivery_history, viewGroup, false);
        return new DeliveredHistoryHolder(view);
    }

    @Override
    public void onBindViewHolder(DeliveredHistoryHolder deliveredHistoryHolder, final int position) {

        setAnimation(deliveredHistoryHolder.itemView, position);
        deliveredHistoryHolder.bind(historyModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.historyModels.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new DeliveredHistorySearch(filterList, this);
        }

        return filter;
    }

    public class DeliveredHistoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView bike_img, status_img;
        TextView status_txt, booking_txt, brand_txt, services_txt, price_txt, last_service_date_txt, sst_address_txt,moving_txt;
        HistoryItemClickListener historyItemClickListener;
        String bookingId, bookingTime, bookingDate, status, brandId, modelId, brandName, modelName, sstName, sstAddress;


        DeliveredHistoryHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            bike_img = itemView.findViewById(R.id.bike_img);
            status_img = itemView.findViewById(R.id.status_img);
            status_txt = itemView.findViewById(R.id.status_txt);
            booking_txt = itemView.findViewById(R.id.booking_txt);
            sst_address_txt = itemView.findViewById(R.id.sst_address_txt);
            brand_txt = itemView.findViewById(R.id.brand_txt);
            services_txt = itemView.findViewById(R.id.services_txt);
            price_txt = itemView.findViewById(R.id.price_txt);
            last_service_date_txt = itemView.findViewById(R.id.last_service_date_txt);
            moving_txt = itemView.findViewById(R.id.moving_txt);

        }

        void bind(final DeliveredHistoryModel historyModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            brandId = historyModel.getBrandId();
            modelId = historyModel.getBrandId();
            brandName = historyModel.getBrandName();
            modelName = historyModel.getModelName();
            sstName = historyModel.getServiceCenterName();
            sstAddress = historyModel.getPickupAddress();

            bookingId = historyModel.getBookingId();
            bookingTime = historyModel.getBookingTime();
            bookingDate = historyModel.getBookingDate();
            status = historyModel.getStatus();

            if (historyModel.isMovingCondition()){
                moving_txt.setTextColor(ContextCompat.getColor(context, R.color.green));
                moving_txt.setText("Vehicle Is In Moving Condition");
            }else {
                moving_txt.setTextColor(ContextCompat.getColor(context, R.color.red));
                moving_txt.setText("Vehicle Is Not In Moving Condition");
            }

            booking_txt.setTypeface(bold);
            booking_txt.setText("Booking Id : " + bookingId);
            sst_address_txt.setTypeface(regular);
            sst_address_txt.setText(sstAddress);
            brand_txt.setTypeface(regular);
            brand_txt.setText(brandName + "\n" + modelName);
            String services = historyModel.getServices();
            String finalString = services.replace(",", "\n");
            services_txt.setTypeface(regular);
            services_txt.setText(finalString);
            price_txt.setTypeface(bold);
            price_txt.setText("\u20B9 " + historyModel.getFinalPrice());
            last_service_date_txt.setTypeface(regular);
            String resultDate = convertStringDateToAnotherStringDate(historyModel.getBookingDate(), "yyyy-MM-dd", "dd-MM-yyyy");
            Log.d("ResultDate", resultDate);

            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            Date d = null;
            try {
                d = df.parse(bookingTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(d);
            cal.add(Calendar.HOUR, 5);
            cal.add(Calendar.MINUTE, 30);
            String newTime = df.format(cal.getTime());

            try {
                SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                Date date = dateFormatter.parse(newTime);
                SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                newTime = timeFormatter.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            last_service_date_txt.setText("Booking Time " + resultDate + " " + newTime);
            status_txt.setTypeface(regular);

            itemView.setOnClickListener(view -> {

                Intent intent = new Intent(context, BookingDetailActivity.class);
                intent.putExtra("activity", "DeliveredHistoryAdapter");
                intent.putExtra("bookingId", bookingId);
                context.startActivity(intent);
            });
        }

        @Override
        public void onClick(View view) {

            this.historyItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
