package in.gaadi360.partner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.PickUpValidateActivity;
import in.gaadi360.partner.interfaces.InspectionItemClickListener;
import in.gaadi360.partner.models.InspectionModel;

public class InspectionAdapter extends RecyclerView.Adapter<InspectionAdapter.InspectionHolder> {

    public ArrayList<InspectionModel> inspectionModels;
    PickUpValidateActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public InspectionAdapter(ArrayList<InspectionModel> inspectionModels, PickUpValidateActivity context, int resource) {
        this.inspectionModels = inspectionModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public InspectionHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_inspection, viewGroup, false);
        return new InspectionHolder(view);
    }

    @Override
    public void onBindViewHolder(InspectionHolder inspectionHolder, final int position) {

        setAnimation(inspectionHolder.itemView, position);
        inspectionHolder.bind(inspectionModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.inspectionModels.size();
    }

    public class InspectionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView insp_img;
        TextView insp_name_txt;
        ImageView accept_img, reject_img;
        InspectionItemClickListener inspectionItemClickListener;

        InspectionHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            insp_img = itemView.findViewById(R.id.insp_img);
            insp_name_txt = itemView.findViewById(R.id.insp_name_txt);
            accept_img = itemView.findViewById(R.id.accept_img);
            reject_img = itemView.findViewById(R.id.reject_img);

        }

        void bind(final InspectionModel inspectionModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            insp_name_txt.setTypeface(bold);

            insp_name_txt.setText(inspectionModel.getInspectionName());

            accept_img.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View view) {

                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));

                    inspectionModel.setState("1");

                    //context.getTotalPrice();

                }
            });

            reject_img.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void onClick(View view) {

                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reject));

                    inspectionModel.setState("2");
                    //context.getTotalPrice();

                }
            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.inspectionItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
