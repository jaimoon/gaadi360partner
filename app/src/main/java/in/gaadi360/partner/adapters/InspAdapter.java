package in.gaadi360.partner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.BookingDetailActivity;
import in.gaadi360.partner.interfaces.AddonHistoryItemClickListener;
import in.gaadi360.partner.models.InspectionModel;

public class InspAdapter extends RecyclerView.Adapter<InspAdapter.InspHolder> {

    public List<InspectionModel> inspectionModels;
    BookingDetailActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public InspAdapter(List<InspectionModel> inspectionModels, BookingDetailActivity context, int resource) {
        this.inspectionModels = inspectionModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public InspHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_insp, viewGroup, false);
        return new InspHolder(view);
    }

    @Override
    public void onBindViewHolder(InspHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(inspectionModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.inspectionModels.size();
    }


    public class InspHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView insp_img;
        TextView insp_txt, state_txt;
        AddonHistoryItemClickListener addonHistoryItemClickListener;

        InspHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            insp_img = itemView.findViewById(R.id.insp_img);
            insp_txt = itemView.findViewById(R.id.insp_txt);
            state_txt = itemView.findViewById(R.id.state_txt);

        }

        void bind(final InspectionModel inspectionModels) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            String inspName = inspectionModels.getInspectionName();
            String state = inspectionModels.getState();
            String inspId = inspectionModels.getInspectionId();

            insp_txt.setTypeface(regular);
            state_txt.setTypeface(bold);

            insp_txt.setText(inspName);

            if (state.equalsIgnoreCase("0")){
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                state_txt.setText("Pending");
            }

            if (state.equalsIgnoreCase("1")){
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.green));
                state_txt.setText("Good");
            }

            if (state.equalsIgnoreCase("2")){
                state_txt.setTextColor(ContextCompat.getColor(context, R.color.red));
                state_txt.setText("Bad");
            }

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.addonHistoryItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
