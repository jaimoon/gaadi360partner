package in.gaadi360.partner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.BookingDetailActivity;
import in.gaadi360.partner.interfaces.PickUpItemClickListener;
import in.gaadi360.partner.models.PickUpModel;

public class MechanicAdapter extends RecyclerView.Adapter<MechanicAdapter.PickUpHolder> {

    public List<PickUpModel> pickUpModels;
    BookingDetailActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public MechanicAdapter(List<PickUpModel> pickUpModels, BookingDetailActivity context, int resource) {
        this.pickUpModels = pickUpModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public PickUpHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_pickup, viewGroup, false);
        return new PickUpHolder(view);
    }

    @Override
    public void onBindViewHolder(PickUpHolder pickUpHolder, final int position) {

        setAnimation(pickUpHolder.itemView, position);
        pickUpHolder.bind(pickUpModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.pickUpModels.size();
    }


    public class PickUpHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView name_txt, mobile_txt;
        ImageView select_img;
        PickUpItemClickListener pickUpItemClickListener;

        PickUpHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            name_txt = itemView.findViewById(R.id.name_txt);
            mobile_txt = itemView.findViewById(R.id.mobile_txt);
            select_img = itemView.findViewById(R.id.select_img);

        }

        void bind(final PickUpModel pickUpModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            String name = pickUpModel.getFirstName();
            String mobile = pickUpModel.getUsername();

            name_txt.setTypeface(regular);
            mobile_txt.setTypeface(regular);

            name_txt.setText(name);
            mobile_txt.setText(mobile);

            itemView.setOnClickListener(view -> {

                String id = pickUpModel.getUserId();

                select_img.setVisibility(View.VISIBLE);

                context.assignMechanic(id);

            });
        }

        @Override
        public void onClick(View view) {

            this.pickUpItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }



    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
