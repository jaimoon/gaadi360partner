package in.gaadi360.partner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.gaadi360.partner.R;
import in.gaadi360.partner.models.ServicesModel;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static in.gaadi360.partner.models.ServicesModel.CHILD_TYPE;
import static in.gaadi360.partner.models.ServicesModel.HEADER_TYPE;

public class DropOffCheckListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ServicesModel> servicesModels;
    private Context context;
    private int lastPosition = -1;

    public DropOffCheckListAdapter(List<ServicesModel> servicesModels, Context context) {
        this.servicesModels = servicesModels;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;

        switch (viewType) {

            case HEADER_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header, parent, false);
                return new DropOffCheckListAdapter.HeaderHolder(view);

            case CHILD_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_service_detail, parent, false);
                return new DropOffCheckListAdapter.ChildHolder(view);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ServicesModel object = servicesModels.get(position);

        if (object != null) {

            switch (object.getValue()) {

                case HEADER_TYPE:

                    setAnimation(((HeaderHolder) holder).itemView, position);

                    Log.d(TAG, "HEADER_TYPE: " + object.getServiceName());
                    ((HeaderHolder) holder).txtServiceName.setText(object.getServiceName());

                    break;

                case CHILD_TYPE:

                    setAnimation(((ChildHolder) holder).itemView, position);

                    Log.d(TAG, "CHILD_TYPE: " + object.getServiceItem());
                    ((DropOffCheckListAdapter.ChildHolder) holder).ser_name_txt.setText(object.getPerform() + " " + object.getServiceItem());

                    ((ChildHolder) holder).good_img.setOnClickListener(view -> {

                        ((ChildHolder) holder).bad_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                        ((ChildHolder) holder).good_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));

                        object.setStatus("1");

                        Log.d("CCCCC", object.getStatus());

                    });

                    ((ChildHolder) holder).bad_img.setOnClickListener(view -> {

                        ((ChildHolder) holder).good_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                        ((ChildHolder) holder).bad_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reject));

                        object.setStatus("2");

                        Log.d("CCCCC", object.getStatus());

                    });

                    break;
            }

        }
    }

    @Override
    public int getItemCount() {
        if (servicesModels == null)
            return 0;
        return servicesModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (servicesModels != null) {
            ServicesModel object = servicesModels.get(position);
            if (object != null) {
                return object.getValue();
            }
        }
        return 0;
    }

    public static class HeaderHolder extends RecyclerView.ViewHolder {

        private TextView txtServiceName;

        HeaderHolder(View itemView) {
            super(itemView);

            txtServiceName = itemView.findViewById(R.id.txtServiceName);
        }
    }

    public static class ChildHolder extends RecyclerView.ViewHolder {

        ImageView ser_img, good_img, bad_img;
        TextView ser_name_txt;

        ChildHolder(View itemView) {
            super(itemView);

            ser_img = itemView.findViewById(R.id.ser_img);
            good_img = itemView.findViewById(R.id.good_img);
            bad_img = itemView.findViewById(R.id.bad_img);
            ser_name_txt = itemView.findViewById(R.id.ser_name_txt);
        }

    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
