package in.gaadi360.partner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.DropOffInspectionsActivity;
import in.gaadi360.partner.interfaces.DropOffInspectionsItemClickListener;
import in.gaadi360.partner.models.DropOffInspectionsModel;

public class DropOffInspectionsAdapter extends RecyclerView.Adapter<DropOffInspectionsAdapter.DropOffInspectionsHolder> {

    private ArrayList<DropOffInspectionsModel> dropOffInspectionsModels;
    DropOffInspectionsActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public DropOffInspectionsAdapter(ArrayList<DropOffInspectionsModel> dropOffInspectionsModels, DropOffInspectionsActivity context, int resource) {
        this.dropOffInspectionsModels = dropOffInspectionsModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public DropOffInspectionsHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_inspection, viewGroup, false);
        return new DropOffInspectionsHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(DropOffInspectionsHolder inspectionHolder, final int position) {

        setAnimation(inspectionHolder.itemView, position);
        inspectionHolder.bind(dropOffInspectionsModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.dropOffInspectionsModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class DropOffInspectionsHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView insp_img, accept_img,reject_img;
        public TextView insp_name_txt;

        DropOffInspectionsItemClickListener dropOffInspectionsItemClickListener;


        public DropOffInspectionsHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            insp_img = itemView.findViewById(R.id.insp_img);
            accept_img = itemView.findViewById(R.id.accept_img);
            reject_img = itemView.findViewById(R.id.reject_img);
            insp_name_txt = itemView.findViewById(R.id.insp_name_txt);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        void bind(final DropOffInspectionsModel dropOffInspectionsModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            insp_name_txt.setTypeface(regular);
            insp_name_txt.setText(dropOffInspectionsModel.getInspectionName());

            accept_img.setOnClickListener(view -> {

                reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));

                dropOffInspectionsModel.setStatus("1");

            });

            reject_img.setOnClickListener(view -> {

                accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reject));

                dropOffInspectionsModel.setStatus("2");

            });

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.dropOffInspectionsItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
