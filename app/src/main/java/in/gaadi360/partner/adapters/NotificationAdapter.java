package in.gaadi360.partner.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.BookingDetailActivity;
import in.gaadi360.partner.activities.NotificationActivity;
import in.gaadi360.partner.interfaces.NotificationItemClickListener;
import in.gaadi360.partner.models.NotificationModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.UserSessionManager;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationHolder> {

    public ArrayList<NotificationModel> notificationModels;
    NotificationActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;
    String accessToken, bookingId, notificationId;
    UserSessionManager userSessionManager;
    //SweetAlertDialog sweetAlertDialog;

    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    public NotificationAdapter(ArrayList<NotificationModel> notificationModels, NotificationActivity context, int resource) {
        this.notificationModels = notificationModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public NotificationHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_notification, viewGroup, false);
        return new NotificationHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationHolder notificationHolder, final int position) {

        setAnimation(notificationHolder.itemView, position);
        notificationHolder.bind(notificationModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.notificationModels.size();
    }

    public class NotificationHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        RelativeLayout not_rl;
        ImageView delete_img;
        TextView booking_txt, desp_txt, time_txt;
        NotificationItemClickListener notificationItemClickListener;

        NotificationHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            delete_img = itemView.findViewById(R.id.delete_img);
            not_rl = itemView.findViewById(R.id.not_rl);
            booking_txt = itemView.findViewById(R.id.booking_txt);
            desp_txt = itemView.findViewById(R.id.desp_txt);
            time_txt = itemView.findViewById(R.id.time_txt);

        }

        void bind(final NotificationModel notificationModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            userSessionManager = new UserSessionManager(context);
            HashMap<String, String> userDetails = userSessionManager.getUserDetails();
            accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

            booking_txt.setTypeface(bold);
            desp_txt.setTypeface(regular);
            time_txt.setTypeface(regular);

            booking_txt.setText("Booking ID : " + notificationModel.getBookingId());
            desp_txt.setText(notificationModel.getDescription());
            time_txt.setText(notificationModel.getCreatedTime());

            if (notificationModel.getReadFlag().equalsIgnoreCase("true")) {

                not_rl.setBackgroundResource(R.drawable.row_border);

            } else if (notificationModel.getReadFlag().equalsIgnoreCase("false")) {

                not_rl.setBackgroundResource(R.drawable.rounded_search);
            }

            delete_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    notificationId = notificationModel.getNotificationId();

                    showClearDialog(R.layout.warning_dialog);

                    /*sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE);
                    sweetAlertDialog.setTitleText("Are you sure?");
                    sweetAlertDialog.setContentText("You want to Clear!");
                    sweetAlertDialog.setCancelText("No, Keep It");
                    sweetAlertDialog.setConfirmText("Yes, Clear");
                    sweetAlertDialog.showCancelButton(true);

                    sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                        sweetAlertDialog.cancel();

                        notificationId = notificationModel.getNotificationId();
                        deleteNotification();

                    });

                    sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                    sweetAlertDialog.show();*/

                }
            });

            itemView.setOnClickListener(view -> {

                notificationId = notificationModel.getNotificationId();
                bookingId = notificationModel.getBookingId();

                getReadorUnread();

            });
        }

        private void getReadorUnread() {
            String url = AppUrls.BASE_URL + AppUrls.READ_UNREAD + notificationId + "/read-or-delete?flag=1";

            StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {


                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String code = jsonObject.getString("code");
                        String status = jsonObject.getString("status");
                        String message = jsonObject.getString("message");

                        Intent intent = new Intent(context, BookingDetailActivity.class);
                        intent.putExtra("activity", "NotificationAdapter");
                        intent.putExtra("bookingId", bookingId);
                        context.startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();

                    }
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                            } else if (error instanceof AuthFailureError) {

                            } else if (error instanceof ServerError) {

                            } else if (error instanceof NetworkError) {

                            } else if (error instanceof ParseError) {

                            }
                        }
                    }
            ) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + accessToken);
                    return headers;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(context);
            requestQueue.add(stringRequest);
        }

        @Override
        public void onClick(View view) {

            this.notificationItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void showClearDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(context);
        View layoutView = context.getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setTypeface(regular);
        msg_txt.setText("You want to Clear !");
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Clear");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Keep It");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                deleteNotification();


            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }

    private void deleteNotification() {

        String url = AppUrls.BASE_URL + AppUrls.READ_UNREAD + notificationId + "/read-or-delete?flag=2";

        Log.d("URL",url);

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String code = jsonObject.getString("code");
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    context.getNotification();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        requestQueue.add(stringRequest);
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
