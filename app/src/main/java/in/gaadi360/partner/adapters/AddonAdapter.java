package in.gaadi360.partner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.BookingDetailActivity;
import in.gaadi360.partner.interfaces.AddonHistoryItemClickListener;
import in.gaadi360.partner.models.BookingServicesModel;

public class AddonAdapter extends RecyclerView.Adapter<AddonAdapter.AddonHolder> {

    public List<BookingServicesModel> servicesModels;
    BookingDetailActivity context;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;

    public AddonAdapter(List<BookingServicesModel> servicesModels, BookingDetailActivity context, int resource) {
        this.servicesModels = servicesModels;
        this.context = context;
        this.resource = resource;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public AddonHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_addon, viewGroup, false);
        return new AddonHolder(view);
    }

    @Override
    public void onBindViewHolder(AddonHolder historyHolder, final int position) {

        setAnimation(historyHolder.itemView, position);
        historyHolder.bind(servicesModels.get(position));

    }

    @Override
    public int getItemCount() {
        return this.servicesModels.size();
    }


    public class AddonHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView service_txt, cost_txt, status_txt;
        AddonHistoryItemClickListener addonHistoryItemClickListener;

        AddonHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            service_txt = itemView.findViewById(R.id.service_txt);
            cost_txt = itemView.findViewById(R.id.cost_txt);
            status_txt = itemView.findViewById(R.id.status_txt);

        }

        void bind(final BookingServicesModel servicesModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            String service = servicesModel.getServiceName();
            String cost = servicesModel.getCost();
            String status = servicesModel.getStatus();

            service_txt.setTypeface(regular);
            cost_txt.setTypeface(regular);
            status_txt.setTypeface(bold);

            service_txt.setText(service);
            cost_txt.setText("\u20B9 " + cost);

            if (status.equalsIgnoreCase("0")){
                status_txt.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                status_txt.setText("Pending");
            }

            if (status.equalsIgnoreCase("1")){
                status_txt.setTextColor(ContextCompat.getColor(context, R.color.green));
                status_txt.setText("Approved");
            }

            if (status.equalsIgnoreCase("2")){
                status_txt.setTextColor(ContextCompat.getColor(context, R.color.red));
                status_txt.setText("Rejected");
            }

            itemView.setOnClickListener(view -> {

            });
        }

        @Override
        public void onClick(View view) {

            this.addonHistoryItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
