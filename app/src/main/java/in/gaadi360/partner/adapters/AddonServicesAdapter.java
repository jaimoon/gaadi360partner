package in.gaadi360.partner.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import in.gaadi360.partner.R;
import in.gaadi360.partner.activities.AddonServicesActivity;
import in.gaadi360.partner.filters.AddonServicesSearch;
import in.gaadi360.partner.interfaces.AddonHistoryItemClickListener;
import in.gaadi360.partner.models.AddonServicesModel;
import in.gaadi360.partner.utils.GlobalCalls;

public class AddonServicesAdapter extends RecyclerView.Adapter<AddonServicesAdapter.AddonHolder> implements Filterable {

    public ArrayList<AddonServicesModel> addonServicesModels, filterList;
    AddonServicesActivity context;
    AddonServicesSearch filter;
    private LayoutInflater li;
    private int resource;
    private int lastPosition = -1;
    Typeface regular, bold;
    String serviceIds;

    public AddonServicesAdapter(String serviceIds, ArrayList<AddonServicesModel> addonServicesModels, AddonServicesActivity context, int resource) {
        this.addonServicesModels = addonServicesModels;
        this.filterList = addonServicesModels;
        this.context = context;
        this.resource = resource;
        this.serviceIds = serviceIds;
        li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public AddonHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_addon_services, viewGroup, false);
        return new AddonHolder(view);
    }

    @Override
    public void onBindViewHolder(AddonHolder addonHolder, final int position) {

        setAnimation(addonHolder.itemView, position);
        addonHolder.bind(addonServicesModels.get(position));
        addonHolder.setIsRecyclable(false);

    }

    @Override
    public int getItemCount() {
        return this.addonServicesModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new AddonServicesSearch(filterList, this);
        }

        return filter;
    }

    public class AddonHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView addon_img, accept_img, reject_img;
        TextView addon_name_txt;
        EditText addon_cost_edt;
        AddonHistoryItemClickListener addonHistoryItemClickListener;

        AddonHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            addon_img = itemView.findViewById(R.id.addon_img);
            accept_img = itemView.findViewById(R.id.accept_img);
            reject_img = itemView.findViewById(R.id.reject_img);
            addon_name_txt = itemView.findViewById(R.id.addon_name_txt);
            addon_cost_edt = itemView.findViewById(R.id.addon_cost_edt);

        }

        void bind(final AddonServicesModel addonServicesModel) {

            regular = Typeface.createFromAsset(context.getAssets(), "proxima_nova_regular.otf");
            bold = Typeface.createFromAsset(context.getAssets(), "proxima_nova_bold.otf");

            addon_name_txt.setTypeface(regular);
            addon_name_txt.setText(addonServicesModel.getServiceName());
            addon_cost_edt.setSelection(addon_cost_edt.getText().length());
            addon_cost_edt.setTypeface(bold);
            addon_cost_edt.setText(addonServicesModel.getCost());

            List<String> myList = new ArrayList<String>(Arrays.asList(serviceIds.split(",")));

            if (myList.contains(addonServicesModel.getServiceId())) {

                addon_cost_edt.setEnabled(true);
                accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));
                reject_img.setVisibility(View.INVISIBLE);
                addonServicesModel.setStatus("1");
                context.getTotalPrice();
            }

            if (addonServicesModel.getStatus().equalsIgnoreCase("1")) {
                reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));

            }

            if (addonServicesModel.isAddonService()){
                addon_cost_edt.setEnabled(false);
            }

            addon_cost_edt.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                    addonServicesModel.setStatus("0");
                    context.getTotalPrice();
                    return false;
                }
            });

            addon_cost_edt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                    addon_cost_edt.setFocusable(true);
                    addonServicesModel.setStatus("0");
                    context.getTotalPrice();
                }
            });


            accept_img.setOnClickListener(view -> {

                if (!addon_cost_edt.getText().toString().equalsIgnoreCase("")) {


                    String editCost = addon_cost_edt.getText().toString();
                    addonServicesModel.setCost(editCost);

                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cancel));
                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_select));

                    addonServicesModel.setCost(addon_cost_edt.getText().toString());
                    addonServicesModel.setStatus("1");
                    addon_cost_edt.setSelection(addon_cost_edt.getText().length());
                    addon_cost_edt.setFocusable(false);
                    context.getTotalPrice();
                    notifyDataSetChanged();
                }else {
                    GlobalCalls.showToast("Service cost not to be zero.", context);

                    addonServicesModel.setCost("0");
                    addon_cost_edt.setText("0");
                    addon_cost_edt.setSelection(addon_cost_edt.getText().length());
                    context.getTotalPrice();
                    notifyDataSetChanged();
                }

            });

            reject_img.setOnClickListener(view -> {
                if (!addon_cost_edt.getText().toString().equalsIgnoreCase("")) {
                    accept_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_deselect));
                    reject_img.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_reject));

                    addonServicesModel.setStatus("2");
                    addon_cost_edt.setSelection(addon_cost_edt.getText().length());
                    addon_cost_edt.setFocusable(false);
                    context.getTotalPrice();
                    //notifyDataSetChanged();
                }else {
                    GlobalCalls.showToast("Service cost not to be zero.", context);

                    addonServicesModel.setCost("0");
                    addon_cost_edt.setText("0");
                    addon_cost_edt.setSelection(addon_cost_edt.getText().length());
                    context.getTotalPrice();
                    notifyDataSetChanged();
                }

            });

            itemView.setOnClickListener(view -> {

            });

            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(addon_cost_edt.getWindowToken(), 0);

                    return false;
                }
            });
        }

        @Override
        public void onClick(View view) {

            this.addonHistoryItemClickListener.onItemClick(view, getLayoutPosition());
        }
    }

    private void setAnimation(View viewToAnimate, int position) {
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
