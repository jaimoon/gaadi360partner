package in.gaadi360.partner.interfaces;

import android.view.View;

public interface PhotosItemClickListener
{
   void onItemClick(View v, int pos);
}
