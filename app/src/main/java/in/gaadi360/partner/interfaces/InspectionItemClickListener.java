package in.gaadi360.partner.interfaces;

import android.view.View;

public interface InspectionItemClickListener
{
   void onItemClick(View v, int pos);
}
