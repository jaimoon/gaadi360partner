package in.gaadi360.partner.interfaces;

import android.view.View;

public interface NotificationItemClickListener
{
    void onItemClick(View v, int pos);
}

