package in.gaadi360.partner.interfaces;

import android.view.View;

public interface AddonServicesItemClickListener
{
   void onItemClick(View v, int pos);
}
