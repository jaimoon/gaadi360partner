package in.gaadi360.partner.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.gaadi360.partner.R;
import in.gaadi360.partner.adapters.DropOffInspectionsAdapter;
import in.gaadi360.partner.models.DropOffInspectionsModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;

public class DropOffInspectionsActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, no_history_img;
    TextView toolbar_title, no_history_txt, booking_txt;
    private boolean checkInternet;
    RecyclerView drop_recyclerview;
    Button submit_btn;

    UserSessionManager userSessionManager;
    String accessToken, bookingId;

    /*DropOff Inspections*/
    DropOffInspectionsAdapter dropOffInspectionsAdapter;
    ArrayList<DropOffInspectionsModel> dropOffInspectionsModels = new ArrayList<DropOffInspectionsModel>();
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_off_inspections);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        no_history_img = findViewById(R.id.no_history_img);
        no_history_txt = findViewById(R.id.no_history_txt);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        booking_txt = findViewById(R.id.booking_txt);
        booking_txt.setText("Booking Id : " + bookingId);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);

        drop_recyclerview = findViewById(R.id.drop_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        drop_recyclerview.setLayoutManager(layoutManager);
        dropOffInspectionsAdapter = new DropOffInspectionsAdapter(dropOffInspectionsModels, DropOffInspectionsActivity.this, R.layout.row_inspection);

        getDropOffInspections();

    }

    private void getDropOffInspections() {
        String url = AppUrls.BASE_URL + AppUrls.DROP_OFF_INSPECTIONS;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        dropOffInspectionsModels.clear();

                        try {
                            JSONObject jsonObject = new JSONObject(response);

                            String responceCode = jsonObject.getString("status");

                            if (responceCode.equals("2000")) {
                                JSONArray jsonArray = jsonObject.getJSONArray("data");

                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    DropOffInspectionsModel im = new DropOffInspectionsModel();
                                    im.setInspectionId(jsonObject1.getString("inspectionId"));
                                    im.setInspectionName(jsonObject1.getString("inspectionName"));
                                    im.setFileId(AppUrls.IMAGE_URL + jsonObject1.getString("fileId"));
                                    dropOffInspectionsModels.add(im);
                                }

                                drop_recyclerview.setAdapter(dropOffInspectionsAdapter);
                                dropOffInspectionsAdapter.notifyDataSetChanged();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        );
        RequestQueue requestQueue = Volley.newRequestQueue(DropOffInspectionsActivity.this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            Intent intent = new Intent(DropOffInspectionsActivity.this, BookingDetailActivity.class);
            intent.putExtra("activity", "AddonServicesActivity");
            intent.putExtra("bookingId", bookingId);
            startActivity(intent);
        }

        if (v == submit_btn) {

            boolean status = false;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {

                status = dropOffInspectionsModels.stream().anyMatch(t -> t.getStatus().equalsIgnoreCase("0"));

            }
            if (!status) {

                delivered();

            } else {

                GlobalCalls.showToast("Please Select All Inspections..!", DropOffInspectionsActivity.this);
            }

        }
    }

    private void delivered() {
        String url = AppUrls.BASE_URL + AppUrls.DELIVERED + bookingId + "/delivered";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);

            JSONArray jArray = new JSONArray();

            for (int i = 0; i < dropOffInspectionsModels.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("inspectionId", dropOffInspectionsModels.get(i).getInspectionId());
                jsonObject1.put("state", dropOffInspectionsModels.get(i).getStatus());
                jsonObject1.put("inspectionName", dropOffInspectionsModels.get(i).getInspectionName());
                jArray.put(jsonObject1);

            }

            jsonObject.put("dropOffInspections", jArray);

            Log.d("ARRAYDATA", "" + jArray);
            Log.d("JAYYYY", "" + jsonObject);

        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    String code = jsonObj.getString("code");

                    GlobalCalls.showToast(message, DropOffInspectionsActivity.this);

                    Intent intent = new Intent(DropOffInspectionsActivity.this, DeliveredActivity.class);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, error -> error.printStackTrace()) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(DropOffInspectionsActivity.this);
        requestQueue.add(request);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(DropOffInspectionsActivity.this, BookingDetailActivity.class);
        intent.putExtra("activity", "AddonServicesActivity");
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);
    }
}
