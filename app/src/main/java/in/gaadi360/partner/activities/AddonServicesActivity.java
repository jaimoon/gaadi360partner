package in.gaadi360.partner.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import org.apache.commons.codec.binary.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import in.gaadi360.partner.R;
import in.gaadi360.partner.adapters.AddonServicesAdapter;
import in.gaadi360.partner.models.AddonServicesModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;

public class AddonServicesActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, no_history_img;
    TextView toolbar_title, no_history_txt, price_txt;
    private boolean checkInternet;
    RecyclerView addon_recyclerview;
    View view1, view2;
    Button submit_btn;

    UserSessionManager userSessionManager;
    String accessToken, addonCost, serviceIds, jsonObj;
    SearchView addon_search;

    /*History*/
    AddonServicesAdapter addonServicesAdapter;
    ArrayList<AddonServicesModel> addonServicesModels = new ArrayList<AddonServicesModel>();
    Typeface regular, bold;
    String bookingId, serviceCenterId, userId,serviceStr,addOnStr;

    JSONArray alreadyServiceArray,alreadyAddedArray;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    FlipProgressDialog flipProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addon_services);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        serviceCenterId = getIntent().getStringExtra("serviceCenterId");
        bookingId = getIntent().getStringExtra("bookingId");
        userId = getIntent().getStringExtra("userId");
        serviceIds = getIntent().getStringExtra("serviceIds");
        jsonObj = getIntent().getStringExtra("jsonObj");

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonObj);

            if (jsonObject.has("services")) {
                alreadyServiceArray = jsonObject.getJSONArray("services");

                Log.d("JSONARRAY1", alreadyServiceArray.toString());
            }

            if (jsonObject.has("bookingAddonServices")) {
                alreadyAddedArray = jsonObject.getJSONArray("bookingAddonServices");
                Log.d("JSONARRAY2", alreadyAddedArray.toString());
            }

            Log.d("JSONARRAY3", alreadyAddedArray.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);


        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        no_history_img = findViewById(R.id.no_history_img);
        no_history_txt = findViewById(R.id.no_history_txt);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        price_txt = findViewById(R.id.price_txt);
        price_txt.setTypeface(bold);

        view1 = findViewById(R.id.view1);
        view2 = findViewById(R.id.view2);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);

        addon_recyclerview = findViewById(R.id.addon_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        addon_recyclerview.setLayoutManager(layoutManager);
        addonServicesAdapter = new AddonServicesAdapter(serviceIds, addonServicesModels, AddonServicesActivity.this, R.layout.row_addon_services);

        getAddons();

        getTotalPrice();

        addon_search = findViewById(R.id.addon_search);
        EditText searchEditText = addon_search.findViewById(androidx.appcompat.R.id.search_src_text);
        addon_search.setOnClickListener(v -> addon_search.setIconified(false));
        addon_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                addonServicesAdapter.getFilter().filter(query);
                addonServicesAdapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    public void getAddons() {
        String url = AppUrls.BASE_URL + AppUrls.ADDON_SERVICES + serviceCenterId + "/services";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                addonServicesModels.clear();

                try {

                    JSONArray jsonArray = new JSONArray(response);


                    if (jsonArray.length() != 0) {

                        addon_search.setVisibility(View.VISIBLE);
                        no_history_img.setVisibility(View.GONE);
                        no_history_txt.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            AddonServicesModel asm = new AddonServicesModel();
                            asm.setServiceId(jsonObject.optString("serviceId"));
                            asm.setServiceName(jsonObject.optString("serviceName"));
                            asm.setDeleted(jsonObject.optString("deleted"));
                            asm.setCreatedTime(jsonObject.optString("createdTime"));
                            asm.setModifiedTime(jsonObject.optString("modifiedTime"));
                            asm.setCost(jsonObject.optString("cost"));

                            for (int k = 0; k < alreadyServiceArray.length(); k++) {
                                JSONObject jsonObject2 = alreadyServiceArray.getJSONObject(k);
                                String serviceId = jsonObject2.optString("serviceId");
                                String cost = jsonObject2.optString("cost");

                                if (jsonObject.optString("serviceId").equalsIgnoreCase(serviceId)) {
                                    asm.setCost(cost);
                                }

                                Log.d("ALREADY", serviceId + "->" + cost);

                            }

                            for (int j = 0; j < alreadyAddedArray.length(); j++) {
                                JSONObject jsonObject1 = alreadyAddedArray.getJSONObject(j);
                                String serviceId = jsonObject1.optString("serviceId");
                                String cost = jsonObject1.optString("cost");
                                String status = jsonObject1.optString("status");

                                if (jsonObject.optString("serviceId").equalsIgnoreCase(serviceId)) {
                                    asm.setCost(cost);
                                    if (!status.equalsIgnoreCase("2")) {
                                        asm.setAddonService(true);
                                    }
                                }

                                Log.d("ALREADY", serviceId + "->" + cost);

                            }

                            addonServicesModels.add(asm);
                        }

                        addon_recyclerview.setAdapter(addonServicesAdapter);
                        addonServicesAdapter.notifyDataSetChanged();
                    } else {
                        addon_search.setVisibility(View.GONE);
                        no_history_txt.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(AddonServicesActivity.this);
        requestQueue.add(stringRequest);
    }

    public void getTotalPrice() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            addonCost = "" + addonServicesModels.stream().filter(t -> t.getStatus().equalsIgnoreCase("1")).mapToDouble(t -> Double.valueOf(t.getCost())).sum();
        }

        view1.setVisibility(View.VISIBLE);
        view2.setVisibility(View.VISIBLE);
        price_txt.setVisibility(View.VISIBLE);

        Log.d("ADDONPRICE",addonCost);
        price_txt.setText("Final Price : \u20B9 " + addonCost);
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            Intent intent = new Intent(AddonServicesActivity.this, BookingDetailActivity.class);
            intent.putExtra("activity", "AddonServicesActivity");
            intent.putExtra("bookingId", bookingId);
            startActivity(intent);
        }

        if (v == submit_btn) {

            /*long count = 0L;
            String[] split = serviceIds.split(",");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                List<String> validate = Arrays.stream(split).filter(t -> !t.equalsIgnoreCase("")).collect(Collectors.toList());
                count = addonServicesModels.stream().filter(t -> t.getStatus().equalsIgnoreCase("1")).count();

                if (Long.valueOf(validate.size()) < count) {
                    sendAddon();
                } else {
                    GlobalCalls.showToast("Please Add Services..!", AddonServicesActivity.this);
                }
            }*/

            sendAddon();

        }
    }

    private void sendAddon() {

        flipProgressDialog.show(getFragmentManager(), "");

        if (checkInternet) {

            String url = AppUrls.BASE_URL + AppUrls.SEND_ADDON;

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("bookingId", bookingId);
                jsonObject.put("serviceCenterId", serviceCenterId);
                jsonObject.put("finalPrice", addonCost);
                jsonObject.put("userId", userId);

                JSONArray jArray = new JSONArray();

                for (int i = 0; i < addonServicesModels.size(); i++) {
                    if (addonServicesModels.get(i).getStatus().equalsIgnoreCase("1")) {
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("serviceId", addonServicesModels.get(i).getServiceId());
                        jsonObject1.put("serviceName", addonServicesModels.get(i).getServiceName());
                        jsonObject1.put("cost", addonServicesModels.get(i).getCost());
                        jArray.put(jsonObject1);
                    }
                }

                jsonObject.put("services", jArray);

                Log.d("ARRAYDATA", "" + jArray);
                Log.d("JAYYYY", "" + jsonObject);

            } catch (Exception e) {
                e.printStackTrace();
            }

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("SuccessResp", response.toString());

                    try {
                        JSONObject jsonObj = new JSONObject(response.toString());
                        String status = jsonObj.getString("status");
                        String message = jsonObj.getString("message");
                        String code = jsonObj.getString("code");

                        flipProgressDialog.dismiss();
                        GlobalCalls.showToast(message, AddonServicesActivity.this);

                        Intent intent = new Intent(AddonServicesActivity.this, BookingDetailActivity.class);
                        intent.putExtra("activity", "AddonServicesActivity");
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, error ->
            {

                error.printStackTrace();
                flipProgressDialog.dismiss();
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                } else if (error instanceof AuthFailureError) {

                } else if (error instanceof ServerError) {

                } else if (error instanceof NetworkError) {

                } else if (error instanceof ParseError) {

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Authorization", "Bearer " + accessToken);
                    return headers;
                }
            };
            request.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            RequestQueue requestQueue = Volley.newRequestQueue(AddonServicesActivity.this);
            requestQueue.add(request);
        }else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(AddonServicesActivity.this, BookingDetailActivity.class);
        intent.putExtra("activity", "AddonServicesActivity");
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);
    }
}
