package in.gaadi360.partner.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.gaadi360.partner.MainActivity;
import in.gaadi360.partner.R;
import in.gaadi360.partner.adapters.InspectionAdapter;
import in.gaadi360.partner.models.InspectionModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;
import in.gaadi360.partner.utils.VolleyMultipartRequest;

public class PickUpValidateActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    UserSessionManager userSessionManager;
    TextView toolbar_title,insp_txt, photo_txt;
    EditText reg_edt, remarks_edt;
    ImageView close, bike_img_one, bike_img_two, bike_img_three, bike_img_four, bike_img_five, bike_img_six;
    Button submit_btn;
    RecyclerView insp_recyclerview;
    Typeface regular, bold;
    String accessToken, bookingId;

    InspectionAdapter inspectionAdapter;
    ArrayList<InspectionModel> inspectionModels = new ArrayList<InspectionModel>();

    ArrayList<String> list = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up_validate);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");

        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        insp_txt = findViewById(R.id.insp_txt);
        insp_txt.setTypeface(bold);
        photo_txt = findViewById(R.id.photo_txt);
        photo_txt.setTypeface(bold);
        reg_edt = findViewById(R.id.reg_edt);
        reg_edt.setTypeface(regular);
        remarks_edt = findViewById(R.id.remarks_edt);
        remarks_edt.setTypeface(regular);
        bike_img_one = findViewById(R.id.bike_img_one);
        bike_img_one.setOnClickListener(this);
        bike_img_two = findViewById(R.id.bike_img_two);
        bike_img_two.setOnClickListener(this);
        bike_img_three = findViewById(R.id.bike_img_three);
        bike_img_three.setOnClickListener(this);
        bike_img_four = findViewById(R.id.bike_img_four);
        bike_img_four.setOnClickListener(this);
        bike_img_five = findViewById(R.id.bike_img_five);
        bike_img_five.setOnClickListener(this);
        bike_img_six = findViewById(R.id.bike_img_six);
        bike_img_six.setOnClickListener(this);
        submit_btn = findViewById(R.id.submit_btn);
        submit_btn.setOnClickListener(this);

        insp_recyclerview = findViewById(R.id.insp_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        insp_recyclerview.setLayoutManager(layoutManager);
        inspectionAdapter = new InspectionAdapter(inspectionModels, PickUpValidateActivity.this, R.layout.row_inspection);

        getInspections();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 101);
            }
        }
    }

    private void getInspections() {

        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("RESP", response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    String bookingId = jsonObject.optString("bookingId");
                    String userId = jsonObject.optString("userId");
                    String orderId = jsonObject.optString("orderId");
                    String serviceCenterId = jsonObject.optString("serviceCenterId");
                    String bookingType = jsonObject.optString("bookingType");
                    String brandId = jsonObject.optString("brandId");
                    String brandName = jsonObject.optString("brandName");
                    String modelId = jsonObject.optString("modelId");
                    String modelName = jsonObject.optString("modelName");
                    String kilometerRangeId = jsonObject.optString("kilometerRangeId");
                    String bookingDate = jsonObject.optString("bookingDate");
                    String bookingTime = jsonObject.optString("bookingTime");
                    String pickupAddress = jsonObject.optString("pickupAddress");
                    String pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                    String pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                    String pickedUpTime = jsonObject.optString("pickedUpTime");
                    String deliveredTime = jsonObject.optString("deliveredTime");
                    String addressType = jsonObject.optString("addressType");
                    String estimatedCost = jsonObject.optString("estimatedCost");
                    String finalPrice = jsonObject.optString("finalPrice");
                    String promocodeAmount = jsonObject.optString("promocodeAmount");
                    String initialPaidAmount = jsonObject.optString("initialPaidAmount");
                    String paymentStatus = jsonObject.optString("paymentStatus");
                    String status = jsonObject.optString("status");
                    String deleted = jsonObject.optString("deleted");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");
                    String firstName = jsonObject.optString("firstName");
                    String lastName = jsonObject.optString("lastName");
                    String serviceCenterName = jsonObject.optString("serviceCenterName");
                    String contactMobileNumber = jsonObject.optString("contactMobileNumber");
                    String mobile = jsonObject.optString("mobile");
                    String rescheduleCount = jsonObject.optString("rescheduleCount");
                    String paidAmount = jsonObject.optString("paidAmount");
                    String addonsAmount = jsonObject.optString("addonsAmount");


                    if (jsonObject.has("inspections")) {

                        insp_txt.setVisibility(View.VISIBLE);
                        insp_recyclerview.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            InspectionModel im = new InspectionModel();
                            im.setBookingId(jsonObject1.getString("bookingId"));
                            im.setInspectionId(jsonObject1.getString("inspectionId"));
                            im.setState(jsonObject1.getString("state"));
                            im.setInspectionName(jsonObject1.getString("inspectionName"));
                            inspectionModels.add(im);
                        }

                        insp_recyclerview.setAdapter(inspectionAdapter);
                        inspectionAdapter.notifyDataSetChanged();

                    } else {
                        insp_txt.setVisibility(View.GONE);
                        insp_recyclerview.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(PickUpValidateActivity.this);
        requestQueue.add(stringRequest);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View v) {
        if (v == close) {

            Intent intent = new Intent(PickUpValidateActivity.this, MainActivity.class);
            startActivity(intent);
        }

        if (v == bike_img_one) {

            /*From Gallery*/
            /*Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, 100);*/

            /*From Camera*/
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 101);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 101);
            }
        }

        if (v == bike_img_two) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 102);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 102);
            }
        }

        if (v == bike_img_three) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 103);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 103);
            }
        }

        if (v == bike_img_four) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 104);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 104);
            }
        }

        if (v == bike_img_five) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 105);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 105);
            }
        }

        if (v == bike_img_six) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA}, 106);
            } else {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 106);
            }
        }


        if (v == submit_btn) {

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                boolean status = inspectionModels.stream().anyMatch(t -> t.getState().equalsIgnoreCase("0"));

                if (!status) {
                    if (reg_edt.getText().toString().isEmpty()) {

                        reg_edt.setError("Please Enter Reg. Number..!");
                        GlobalCalls.showToast("Please Enter Reg. Number..!", PickUpValidateActivity.this);

                    } /*else if (remarks_edt.getText().toString().isEmpty()) {

                        remarks_edt.setError("Please Enter Remarks..!");
                        GlobalCalls.showToast("Please Enter Remarks..!", PickUpValidateActivity.this);

                    }*/ else {

                        sendInspData();

                    }
                } else {

                    GlobalCalls.showToast("Please Inspect All Services..!", PickUpValidateActivity.this);
                }
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 100) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 100);
            } else {
                Toast.makeText(this, "camera permission denied", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*From Gallery*/
        /*if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            Uri imageUri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
                bike_img.setImageBitmap(bitmap);
                uploadBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/

        /*From Camera*/
        if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bike_img_one.setImageBitmap(photo);

            uploadBitmap(photo, "1");
        }

        if (requestCode == 102 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bike_img_two.setImageBitmap(photo);

            uploadBitmap(photo, "2");
        }

        if (requestCode == 103 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bike_img_three.setImageBitmap(photo);

            uploadBitmap(photo, "3");
        }

        if (requestCode == 104 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bike_img_four.setImageBitmap(photo);

            uploadBitmap(photo, "4");
        }

        if (requestCode == 105 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bike_img_five.setImageBitmap(photo);

            uploadBitmap(photo, "5");
        }

        if (requestCode == 106 && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            bike_img_six.setImageBitmap(photo);

            uploadBitmap(photo, "6");
        }
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void uploadBitmap(final Bitmap bitmap, String count) {

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, AppUrls.BASE_URL + AppUrls.SEND_BIKE_IMAGE,
                new Response.Listener<NetworkResponse>() {

                    @Override
                    public void onResponse(NetworkResponse response) {

                        GlobalCalls.showToast("Success", PickUpValidateActivity.this);

                        if (count.equalsIgnoreCase("1")) {
                            bike_img_two.setVisibility(View.VISIBLE);
                        } else if (count.equalsIgnoreCase("2")) {
                            bike_img_three.setVisibility(View.VISIBLE);
                        } else if (count.equalsIgnoreCase("3")) {
                            bike_img_four.setVisibility(View.VISIBLE);
                        } else if (count.equalsIgnoreCase("4")) {
                            bike_img_five.setVisibility(View.VISIBLE);
                        } else if (count.equalsIgnoreCase("5")) {
                            bike_img_six.setVisibility(View.VISIBLE);
                        }

                        try {
                            String json = new String(response.data, HttpHeaderParser.parseCharset(response.headers));

                            JSONObject jsonObject = new JSONObject(json);
                            String fileId = jsonObject.optString("fileId");
                            String mimeType = jsonObject.optString("mimeType");
                            String fileName = jsonObject.optString("fileName");
                            String filePath = jsonObject.optString("filePath");
                            String fileSize = jsonObject.optString("fileSize");
                            String checksum = jsonObject.optString("checksum");
                            String createdTime = jsonObject.optString("createdTime");
                            String modifiedTime = jsonObject.optString("modifiedTime");

                            Log.d("ImgData", fileId + "\n" + mimeType + "\n" + fileName + "\n" + filePath + "\n" + fileSize + "\n" +
                                    checksum + "\n" + createdTime + "\n" + modifiedTime);

                            list.add(fileId);

                            Log.d("LIST", String.valueOf(list));

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // params.put("tags", tags);
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                long imagename = System.currentTimeMillis();
                params.put("file", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    private void sendInspData() {

        String url = AppUrls.BASE_URL + AppUrls.SEND_PICKUP;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("bookingId", bookingId);
            JSONArray jArray = new JSONArray();

            for (int i = 0; i < inspectionModels.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("bookingId", inspectionModels.get(i).getBookingId());
                jsonObject1.put("inspectionId", inspectionModels.get(i).getInspectionId());
                jsonObject1.put("state", inspectionModels.get(i).getState());
                jsonObject1.put("inspectionName", inspectionModels.get(i).getInspectionName());
                jArray.put(jsonObject1);
            }
            jsonObject.put("inspections", jArray);

            JSONArray jsArray = new JSONArray(list);
            jsonObject.put("fileIds", jsArray);

            jsonObject.put("registrationNumber", reg_edt.getText().toString());
            jsonObject.put("inspectionWords", remarks_edt.getText().toString());

            Log.d("Array", "" + jArray);
            Log.d("ServicesObject", "" + jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    String code = jsonObject.getString("code");

                    Intent intent = new Intent(PickUpValidateActivity.this, BookingDetailActivity.class);
                    intent.putExtra("activity", "BookingDetailActivity");
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(PickUpValidateActivity.this);
        requestQueue.add(request);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(PickUpValidateActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
