package in.gaadi360.partner.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import java.lang.reflect.Field;

import cn.pedant.SweetAlert.SweetAlertDialog;
import in.gaadi360.partner.MainActivity;
import in.gaadi360.partner.R;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    ImageView close;
    TextView toolbar_title, acc_txt, about_txt, t_txt, help_txt, privacy_txt, can_txt, logout_txt,version_txt;
    RelativeLayout my_accountRL, aboutUsRL, tcRL, help_centerRL, privacy_policyRL, refund_policyRL, logoutRL;
    UserSessionManager userSessionManager;
    SweetAlertDialog sweetAlertDialog;
    Typeface regular, bold;
    String versionName;
    int versionCode;
    AlertDialog.Builder dialogBuilder;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        acc_txt = findViewById(R.id.acc_txt);
        acc_txt.setTypeface(regular);
        about_txt = findViewById(R.id.about_txt);
        about_txt.setTypeface(regular);
        t_txt = findViewById(R.id.t_txt);
        t_txt.setTypeface(regular);
        help_txt = findViewById(R.id.help_txt);
        help_txt.setTypeface(regular);
        privacy_txt = findViewById(R.id.privacy_txt);
        privacy_txt.setTypeface(regular);
        can_txt = findViewById(R.id.can_txt);
        can_txt.setTypeface(regular);
        logout_txt = findViewById(R.id.logout_txt);
        logout_txt.setTypeface(regular);

        my_accountRL = findViewById(R.id.my_accountRL);
        my_accountRL.setOnClickListener(this);
        aboutUsRL = findViewById(R.id.aboutUsRL);
        aboutUsRL.setOnClickListener(this);
        tcRL = findViewById(R.id.tcRL);
        tcRL.setOnClickListener(this);
        help_centerRL = findViewById(R.id.help_centerRL);
        help_centerRL.setOnClickListener(this);
        privacy_policyRL = findViewById(R.id.privacy_policyRL);
        privacy_policyRL.setOnClickListener(this);
        refund_policyRL = findViewById(R.id.refund_policyRL);
        refund_policyRL.setOnClickListener(this);
        logoutRL = findViewById(R.id.logoutRL);
        logoutRL.setOnClickListener(this);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = pInfo.versionName;
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        StringBuilder builder = new StringBuilder();
        builder.append("Android : ").append(Build.VERSION.RELEASE);

        Field[] fields = new Field[0];
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.DONUT) {
            fields = Build.VERSION_CODES.class.getFields();
        }
        for (Field field : fields) {
            String fieldName = field.getName();
            int fieldValue = -1;

            try {
                fieldValue = field.getInt(new Object());
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (fieldValue == Build.VERSION.SDK_INT) {
                builder.append("(").append(fieldName).append(")");
                //builder.append("sdk=").append(fieldValue);
            }
        }

        Log.d("OS: ", builder.toString());

        version_txt = findViewById(R.id.version_txt);
        version_txt.setTypeface(regular);
        version_txt.setText("Version " + versionName + " - (" + builder.toString() + ")");
    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == my_accountRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, MyAccountActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == aboutUsRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, AboutUsActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == tcRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, TermsAndConditionsActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == help_centerRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, HelpCenterActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == privacy_policyRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, PrivacyAndPolicyActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == refund_policyRL) {
            if (checkInternet) {
                Intent intent = new Intent(SettingActivity.this, CancellationAndRefundPolicyActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == logoutRL) {
            if (checkInternet) {

                showLogoutDialog(R.layout.warning_dialog);

                /*sweetAlertDialog = new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.WARNING_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setCancelText("No, Stay");
                sweetAlertDialog.setConfirmText("Yes, Log Out");
                sweetAlertDialog.showCancelButton(true);

                sweetAlertDialog.setConfirmClickListener(sweetAlertDialog -> {

                    sweetAlertDialog.cancel();
                    userSessionManager.logoutUser();

                    Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                });

                sweetAlertDialog.setCancelClickListener(sDialog -> sDialog.cancel());
                sweetAlertDialog.show();*/

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void showLogoutDialog(int layout) {
        dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(SettingActivity.this);
        View layoutView = getLayoutInflater().inflate(layout, null);
        TextView title_txt = layoutView.findViewById(R.id.title_txt);
        title_txt.setTypeface(bold);
        title_txt.setText("Are you sure?");
        TextView msg_txt = layoutView.findViewById(R.id.msg_txt);
        msg_txt.setVisibility(View.GONE);
        Button ok_btn = layoutView.findViewById(R.id.ok_btn);
        ok_btn.setTypeface(bold);
        ok_btn.setText("Yes, Log Out");
        Button cancel_btn = layoutView.findViewById(R.id.cancel_btn);
        cancel_btn.setTypeface(bold);
        cancel_btn.setText("No, Stay");
        dialogBuilder.setView(layoutView);
        alertDialog = dialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.show();

        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

                userSessionManager.logoutUser();

                Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();

            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alertDialog.dismiss();

            }
        });
        alertDialog.setCancelable(false);
    }


    @Override
    public void onBackPressed() {
        Intent intent = new Intent(SettingActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
