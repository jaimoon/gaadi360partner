package in.gaadi360.partner.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import com.stfalcon.smsverifycatcher.OnSmsCatchListener;
import com.stfalcon.smsverifycatcher.SmsVerifyCatcher;
import com.taishi.flipprogressdialog.FlipProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import in.aabhasjindal.otptextview.OtpTextView;
import in.gaadi360.partner.MainActivity;
import in.gaadi360.partner.R;
import in.gaadi360.partner.models.ServiceTypeImageModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.SliderAdapterExample;
import in.gaadi360.partner.utils.SliderItem;
import in.gaadi360.partner.utils.UserSessionManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private boolean checkInternet;
    TextInputLayout mobile_til;
    EditText mobile_edt;
    Button login_btn;
    OtpTextView otpTextView;
    SmsVerifyCatcher smsVerifyCatcher;

    Typeface regular, bold;
    String mobile, otp, firstName, username, authority, email, createdTime, modifiedTime, accessToken, mediaSecret, fcmToken, imei, user_image;
    Boolean locked, enabled, expired, emailVerified, mobileVerified;
    Integer userId;
    UserSessionManager userSessionManager;
    AlertDialog dialog;
    private Boolean exit = false;

    SliderView sliderView;
    private SliderAdapterExample adapter;

    /*ProgressDialog*/
    List<Integer> imageList = new ArrayList<Integer>();
    FlipProgressDialog flipProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        checkInternet = NetworkChecking.isConnected(this);
        userSessionManager = new UserSessionManager(this);
        imei = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.d("IMEI", imei);

        sliderView = findViewById(R.id.imageSlider);

        adapter = new SliderAdapterExample(this);
        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.setScrollTimeInSec(3);
        sliderView.setAutoCycle(true);
        sliderView.startAutoCycle();

        imageList.add(R.mipmap.ic_launcher);
        flipProgressDialog = new FlipProgressDialog();
        flipProgressDialog.setImageList(imageList);
        flipProgressDialog.setCanceledOnTouchOutside(true);
        flipProgressDialog.setDimAmount(0.8f); //0.0f
        flipProgressDialog.setBackgroundColor(Color.parseColor("#00FFFFFF"));
        flipProgressDialog.setBackgroundAlpha(0.2f);
        flipProgressDialog.setBorderStroke(0);
        flipProgressDialog.setBorderColor(-1);
        flipProgressDialog.setCornerRadius(16);
        flipProgressDialog.setImageSize(200);
        flipProgressDialog.setImageMargin(10);
        flipProgressDialog.setOrientation("rotationY");
        flipProgressDialog.setDuration(600);
        flipProgressDialog.setStartAngle(0.0f);
        flipProgressDialog.setEndAngle(180.0f);
        flipProgressDialog.setMinAlpha(0.0f);
        flipProgressDialog.setMaxAlpha(1.0f);

        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                fcmToken = instanceIdResult.getToken();
                Log.d("TOKEN", fcmToken);
            }
        });

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        mobile_til = findViewById(R.id.mobile_til);
        mobile_edt = findViewById(R.id.mobile_edt);
        mobile_edt.setTypeface(regular);

        login_btn = findViewById(R.id.login_btn);
        login_btn.setTypeface(bold);
        login_btn.setOnClickListener(this);

        getServiceType();

    }

    private void getServiceType() {
        if (checkInternet) {
            //flipProgressDialog.show(getFragmentManager(), "");
            String url = AppUrls.BASE_URL + AppUrls.SERVICETYPE;

            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String status = jsonObject.getString("status");

                                if (status.equalsIgnoreCase("2000")) {
                                    //flipProgressDialog.dismiss();

                                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                                    JSONArray jsonArray = jsonObject1.getJSONArray("files");
                                    JSONArray jsonArray1 = jsonObject1.getJSONArray("services");

                                    List<SliderItem> sliderItemList = new ArrayList<>();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                        ServiceTypeImageModel serviceTypeImageModel = new ServiceTypeImageModel();
                                        serviceTypeImageModel.setDesc(jsonObject2.optString("desc"));
                                        serviceTypeImageModel.setFileId(AppUrls.IMAGE_URL + jsonObject2.getString("fileId"));

                                        SliderItem sliderItem = new SliderItem();

                                        user_image = AppUrls.IMAGE_URL + jsonObject2.getString("fileId");

                                        sliderItem.setImageUrl(user_image);
                                        sliderItem.setDescription(jsonObject1.optString("desc"));
                                        sliderItemList.add(sliderItem);
                                    }

                                    adapter.renewItems(sliderItemList);

                                } else {

                                    Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Invalid Data..!", Snackbar.LENGTH_LONG);
                                    snackbar.show();

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.getMessage();
                        }
                    });

            RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
            requestQueue.add(stringRequest);
        } else {
            Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == login_btn) {
            login();
        }
    }

    private boolean validateMobile() {
        boolean result = true;
        if (mobile.length() != 10) {
            mobile_til.setError("Invalid Mobile Number");
            result = false;
        } else {
            mobile_til.setError(null);
        }
        return result;
    }

    private void login() {

        mobile = mobile_edt.getText().toString();
        if (validateMobile()) {
            if (checkInternet) {
                flipProgressDialog.show(getFragmentManager(), "");
                String url = AppUrls.BASE_URL + AppUrls.LOGIN + mobile;
                Log.d("LOGINURL", url);
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    Log.d("LOGIN", response);
                                    String status = jsonObject.getString("status");
                                    String message = jsonObject.optString("message");

                                    if (status.equalsIgnoreCase("2000")) {

                                        flipProgressDialog.dismiss();

                                        otpDialog();

                                    } else {
                                        flipProgressDialog.dismiss();
                                        GlobalCalls.showToast(message, LoginActivity.this);

                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                flipProgressDialog.dismiss();

                                if (error == null || error.networkResponse == null) {
                                    return;
                                }

                                try {
                                    String body = new String(error.networkResponse.data, "UTF-8");

                                    JSONObject jsonObject = new JSONObject(body);

                                    GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                } catch (Exception e) {
                                    GlobalCalls.showToast("You Are Not Registered..!", LoginActivity.this);
                                }

                                //GlobalCalls.showToast("You Are Not registered..!",LoginActivity.this);
                            }
                        });

                RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                requestQueue.add(stringRequest);
            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void otpDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setCancelable(false);
        LayoutInflater inflater = LoginActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.login_otp_dialog, null);

        TextView otp_txt = dialog_layout.findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        otpTextView = dialog_layout.findViewById(R.id.otpTextView);
        TextView resend_txt = dialog_layout.findViewById(R.id.resend_txt);
        resend_txt.setTypeface(regular);
        resend_txt.setPaintFlags(resend_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();

        resend_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flipProgressDialog.show(getFragmentManager(), "");
                //String url = AppUrls.BASE_URL + AppUrls.LOGIN + mobile;
                String URL = AppUrls.BASE_URL + AppUrls.CALL_OTP + mobile + "&retrytype=text";
                mobile = mobile_edt.getText().toString();
                otpTextView.setOTP("");
                if (validateMobile()) {
                    if (checkInternet) {
                        //flipProgressDialog.show(getFragmentManager(), "");
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            Log.d("LOGIN", response);
                                            String status = jsonObject.getString("status");
                                            String message = jsonObject.getString("message");

                                            if (status.equalsIgnoreCase("2000")) {

                                                flipProgressDialog.dismiss();

                                            } else {

                                                flipProgressDialog.dismiss();
                                                GlobalCalls.showToast(message, LoginActivity.this);

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }

                                        try {
                                            String body = new String(error.networkResponse.data, "UTF-8");

                                            JSONObject jsonObject = new JSONObject(body);

                                            GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                        } catch (Exception e) {
                                            GlobalCalls.showToast("Invalid Details..!", LoginActivity.this);
                                        }
                                    }
                                });

                        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                        requestQueue.add(stringRequest);
                    } else {
                        Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }
            }
        });
        Button verify_btn = dialog_layout.findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                flipProgressDialog.show(getFragmentManager(), "");
                otp = otpTextView.getOTP();
                //String url = AppUrls.BASE_URL + AppUrls.VERIFYOTP + mobile + "&otp=" + otp;
                String url = AppUrls.BASE_URL + AppUrls.VERIFYOTP + mobile + "&otp=" + otp;
                Log.d("VERIFY", url);
                String otp = otpTextView.getOTP();
                if (validate()) {
                    if (checkInternet) {
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject1 = new JSONObject(response);
                                            flipProgressDialog.dismiss();

                                            Log.d("OTPRESP", response);

                                            userId = jsonObject1.optInt("userId");
                                            firstName = jsonObject1.optString("firstName");
                                            username = jsonObject1.optString("username");
                                            locked = jsonObject1.optBoolean("locked");
                                            enabled = jsonObject1.optBoolean("enabled");
                                            expired = jsonObject1.optBoolean("expired");
                                            authority = jsonObject1.optString("authority");
                                            email = jsonObject1.optString("email");
                                            mobile = jsonObject1.optString("mobile");
                                            emailVerified = jsonObject1.optBoolean("emailVerified");
                                            mobileVerified = jsonObject1.optBoolean("mobileVerified");
                                            createdTime = jsonObject1.optString("createdTime");
                                            modifiedTime = jsonObject1.optString("modifiedTime");
                                            accessToken = jsonObject1.optString("accessToken");
                                            mediaSecret = jsonObject1.optString("mediaSecret");

                                            Log.d("LoginDetails", userId + "\n" + firstName + "\n" + username + "\n" + locked + "\n" +
                                                    enabled + "\n" + expired + "\n" + authority + "\n" + email + "\n" + mobile + "\n" + emailVerified +
                                                    "\n" + mobileVerified + "" + "\n" + createdTime + "\n" + modifiedTime + "\n" + accessToken + "\n"
                                                    + mediaSecret);

                                            if (authority.equalsIgnoreCase("ROLE_SERVICE_CENTER")) {

                                                userSessionManager.createUserLoginSession(firstName, username, email, mobile, accessToken, mediaSecret);

                                                sendFCM();

                                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                                startActivity(intent);
                                            } else {
                                                Toast.makeText(LoginActivity.this, "Authentication Failed..!", Toast.LENGTH_SHORT).show();
                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        flipProgressDialog.dismiss();
                                        otpTextView.showError();
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }

                                        try {
                                            String body = new String(error.networkResponse.data, "UTF-8");

                                            JSONObject jsonObject = new JSONObject(body);

                                            GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                        } catch (Exception e) {
                                            GlobalCalls.showToast("Invalid Otp..!", LoginActivity.this);
                                        }

                                    }
                                });
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                        requestQueue.add(stringRequest);
                    } else {
                        Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                        snackbar.show();
                    }
                }
            }
        });

        TextView call_txt = dialog_layout.findViewById(R.id.call_txt);
        call_txt.setVisibility(View.VISIBLE);
        Button call_btn = dialog_layout.findViewById(R.id.call_btn);

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {

                call_txt.setText("Seconds Remaining : " + millisUntilFinished / 1000);

            }

            public void onFinish() {
                call_txt.setText("done!");
                call_txt.setVisibility(View.GONE);
                resend_txt.setVisibility(View.VISIBLE);
                call_btn.setVisibility(View.VISIBLE);

                call_btn.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        call_btn.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.white));
                        call_btn.setBackgroundResource(R.drawable.rounded_orange_border);

                        flipProgressDialog.show(getFragmentManager(), "");
                        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppUrls.BASE_URL + AppUrls.CALL_OTP + mobile + "&retrytype=voice",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            Log.d("CALL", response);

                                            /*call_txt.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.black));
                                            call_btn.setBackgroundResource(R.drawable.searchview_boarder);*/

                                            String status = jsonObject.getString("status");
                                            String message = jsonObject.getString("message");

                                            if (status.equalsIgnoreCase("2000")) {

                                                flipProgressDialog.dismiss();

                                            } else {

                                                flipProgressDialog.dismiss();
                                                GlobalCalls.showToast(message, LoginActivity.this);

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error == null || error.networkResponse == null) {
                                            return;
                                        }

                                        try {
                                            String body = new String(error.networkResponse.data, "UTF-8");

                                            JSONObject jsonObject = new JSONObject(body);

                                            GlobalCalls.showToast(jsonObject.optString("message"), LoginActivity.this);
                                        } catch (Exception e) {
                                            GlobalCalls.showToast("Invalid Details..!", LoginActivity.this);
                                        }
                                    }
                                });
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(500000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
                        requestQueue.add(stringRequest);
                    }
                });

            }

        }.start();

        builder.setView(dialog_layout).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    private boolean validate() {
        boolean result = true;
        //String otp = otpTextView.getText().toString().trim();
        otp = otpTextView.getOTP();
        Log.d("OTP", otp);
        if ((otp == null || otp.equals("")) || otp.length() != 6) {
            //otpTextView.setError("Invalid OTP");
            otpTextView.showError();
            result = false;
        }
        return result;
    }

    private String parseCode(String message) {
        Pattern p = Pattern.compile("\\b\\d{6}\\b");
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    private void sendFCM() {

        String url = AppUrls.BASE_URL + AppUrls.FCM;
        JSONObject jsonObj = new JSONObject();

        Log.d("TOKENNNN", "" + fcmToken);

        try {
            jsonObj.put("platformId", "1");
            jsonObj.put("pushRegId", fcmToken);
            jsonObj.put("imei", imei);
            jsonObj.put("roleType", "2");
            jsonObj.put("userId", userId);

        } catch (Exception e) {

        }

        Log.d("JsonObj", jsonObj.toString());

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("FCMStatus", response.toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");

                    Log.d("Status", status + "\n" + message);

                    if (status.equalsIgnoreCase("2000")) {

                        //Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                    Log.d("ResError", e.getMessage());
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                //Log.d("EROORRR", error.getMessage());

            }
        }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        requestQueue.add(request);
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);

        } else {

            GlobalCalls.showToast("Press Back again to Exit.!", LoginActivity.this);

            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 2 * 1000);
        }
    }
}
