package in.gaadi360.partner.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import in.aabhasjindal.otptextview.OtpTextView;
import in.gaadi360.partner.MainActivity;
import in.gaadi360.partner.R;
import in.gaadi360.partner.adapters.AddonAdapter;
import in.gaadi360.partner.adapters.InspAdapter;
import in.gaadi360.partner.adapters.MechanicAdapter;
import in.gaadi360.partner.adapters.PhotosAdapter;
import in.gaadi360.partner.adapters.PickUpAdapter;
import in.gaadi360.partner.models.BookingServicesModel;
import in.gaadi360.partner.models.InspectionModel;
import in.gaadi360.partner.models.PickUpModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;

public class BookingDetailActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    ImageView close,redirect_img;
    private boolean checkInternet;
    String bookingId, accessToken, brandId, brandName, modelId, modelName, status, startRange, endRange, mobile, otp, serviceCenterId, userId, serviceIds,
            addOnServiceIds, activity, pickupAddressLatitude, pickupAddressLongitude;

    UserSessionManager userSessionManager;
    TextView toolbar_title, booking_txt, brand_txt, model_txt, date_txt, time_txt, service_detail_txt, addon_service_txt, pick_date_txt, pick_time_txt,
            photos_txt, payment_status_txt, pickup_txt, final_price_txt, bike_txt, service_txt, address_txt, est_price_txt, est_txt, tot_txt, init_txt,
            init_price_txt, disc_txt, dicount_txt, addon_txt, addon_price_txt, insp_txt, remarks_txt, remark_txt, moving_txt, kilometers_txt,
            customer_mobile_txt,reg_txt;
    Typeface regular, bold;
    TableRow discount_tr, addon_tr;
    RelativeLayout loc_rl, photo_rl, addon_rl, insp_rl, remarks_rl;
    Button assign_btn, addon_btn, completed_btn;
    LinearLayout addon_ll;

    RecyclerView addon_recyclerview, photos_recyclerview, insp_recyclerview;
    AddonAdapter addonAdapter;
    List<BookingServicesModel> addOnModels = new ArrayList<BookingServicesModel>();
    InspAdapter inspAdapter;
    List<InspectionModel> inspectionModels = new ArrayList<InspectionModel>();
    PhotosAdapter photosAdapter;
    List<String> mylist = new ArrayList<String>();

    AlertDialog dialog;
    PickUpAdapter pickUpAdapter;
    ArrayList<PickUpModel> pickUpModels = new ArrayList<PickUpModel>();
    MechanicAdapter mechanicAdapter;
    ArrayList<PickUpModel> mechanicModels = new ArrayList<PickUpModel>();

    OtpTextView otpTextView;

    JSONObject jsonObject;

    private SwipeRefreshLayout swipe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_detail);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        activity = getIntent().getStringExtra("activity");
        bookingId = getIntent().getStringExtra("bookingId");


        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        swipe = findViewById(R.id.swipe);
        swipe.setColorSchemeResources(R.color.orange,R.color.colorPrimary);
        swipe.setOnRefreshListener(this);

        redirect_img = findViewById(R.id.redirect_img);
        redirect_img.setOnClickListener(this);

        booking_txt = findViewById(R.id.booking_txt);
        booking_txt.setTypeface(bold);
        customer_mobile_txt = findViewById(R.id.customer_mobile_txt);
        customer_mobile_txt.setTypeface(bold);
        customer_mobile_txt.setPaintFlags(customer_mobile_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        customer_mobile_txt.setOnClickListener(this);
        bike_txt = findViewById(R.id.bike_txt);
        bike_txt.setTypeface(bold);
        brand_txt = findViewById(R.id.brand_txt);
        brand_txt.setTypeface(regular);
        model_txt = findViewById(R.id.model_txt);
        model_txt.setTypeface(regular);
        reg_txt = findViewById(R.id.reg_txt);
        reg_txt.setTypeface(regular);
        date_txt = findViewById(R.id.date_txt);
        date_txt.setTypeface(regular);
        time_txt = findViewById(R.id.time_txt);
        time_txt.setTypeface(regular);

        service_txt = findViewById(R.id.service_txt);
        service_txt.setTypeface(bold);
        service_detail_txt = findViewById(R.id.service_detail_txt);
        service_detail_txt.setTypeface(regular);

        remarks_txt = findViewById(R.id.remarks_txt);
        remarks_txt.setTypeface(bold);
        remark_txt = findViewById(R.id.remark_txt);
        remark_txt.setTypeface(regular);

        addon_service_txt = findViewById(R.id.addon_service_txt);
        addon_service_txt.setTypeface(bold);

        insp_recyclerview = findViewById(R.id.insp_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        insp_recyclerview.setLayoutManager(layoutManager);
        inspAdapter = new InspAdapter(inspectionModels, BookingDetailActivity.this, R.layout.row_insp);

        addon_recyclerview = findViewById(R.id.addon_recyclerview);
        RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(getApplicationContext());
        addon_recyclerview.setLayoutManager(layoutManager1);
        addonAdapter = new AddonAdapter(addOnModels, BookingDetailActivity.this, R.layout.row_addon);

        insp_rl = findViewById(R.id.insp_rl);
        remarks_rl = findViewById(R.id.remarks_rl);
        addon_rl = findViewById(R.id.addon_rl);
        photo_rl = findViewById(R.id.photo_rl);
        loc_rl = findViewById(R.id.loc_rl);
        loc_rl.setOnClickListener(this);

        photos_recyclerview = findViewById(R.id.photos_recyclerview);
        photosAdapter = new PhotosAdapter(mylist, BookingDetailActivity.this, R.layout.row_photos);
        photos_recyclerview.setLayoutManager(new GridLayoutManager(this, 1, GridLayoutManager.HORIZONTAL, false));

        address_txt = findViewById(R.id.address_txt);
        address_txt.setTypeface(bold);
        pick_date_txt = findViewById(R.id.pick_date_txt);
        pick_date_txt.setTypeface(regular);
        pick_time_txt = findViewById(R.id.pick_time_txt);
        pick_time_txt.setTypeface(regular);
        pickup_txt = findViewById(R.id.pickup_txt);
        pickup_txt.setTypeface(regular);
        pickup_txt.setOnClickListener(this);

        discount_tr = findViewById(R.id.discount_tr);
        addon_tr = findViewById(R.id.addon_tr);

        est_txt = findViewById(R.id.est_txt);
        est_txt.setTypeface(bold);
        est_price_txt = findViewById(R.id.est_price_txt);
        est_price_txt.setTypeface(bold);

        init_txt = findViewById(R.id.init_txt);
        init_txt.setTypeface(bold);
        init_price_txt = findViewById(R.id.init_price_txt);
        init_price_txt.setTypeface(bold);

        disc_txt = findViewById(R.id.disc_txt);
        disc_txt.setTypeface(bold);
        dicount_txt = findViewById(R.id.dicount_txt);
        dicount_txt.setTypeface(bold);

        insp_txt = findViewById(R.id.insp_txt);
        insp_txt.setTypeface(bold);
        addon_txt = findViewById(R.id.addon_txt);
        addon_txt.setTypeface(bold);
        addon_price_txt = findViewById(R.id.addon_price_txt);
        addon_price_txt.setTypeface(bold);

        tot_txt = findViewById(R.id.tot_txt);
        tot_txt.setTypeface(bold);
        final_price_txt = findViewById(R.id.final_price_txt);
        final_price_txt.setTypeface(bold);

        photos_txt = findViewById(R.id.photos_txt);
        photos_txt.setTypeface(bold);

        payment_status_txt = findViewById(R.id.payment_status_txt);
        payment_status_txt.setTypeface(bold);

        moving_txt = findViewById(R.id.moving_txt);
        moving_txt.setTypeface(bold);

        kilometers_txt = findViewById(R.id.kilometers_txt);
        kilometers_txt.setTypeface(regular);

        assign_btn = findViewById(R.id.assign_btn);
        assign_btn.setTypeface(bold);
        assign_btn.setOnClickListener(this);
        addon_ll = findViewById(R.id.addon_ll);
        addon_btn = findViewById(R.id.addon_btn);
        addon_btn.setTypeface(bold);
        addon_btn.setOnClickListener(this);
        completed_btn = findViewById(R.id.completed_btn);
        completed_btn.setTypeface(bold);
        completed_btn.setOnClickListener(this);

        getHistoryDetail();
    }

    private void getHistoryDetail() {

        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("RESP", response);

                try {
                    jsonObject = new JSONObject(response);

                    String bookingId = jsonObject.optString("bookingId");
                    booking_txt.setText("Booking Id : " + bookingId);
                    userId = jsonObject.optString("userId");
                    String orderId = jsonObject.optString("orderId");
                    serviceCenterId = jsonObject.optString("serviceCenterId");
                    String bookingType = jsonObject.optString("bookingType");
                    brandId = jsonObject.optString("brandId");
                    brandName = jsonObject.optString("brandName");
                    modelId = jsonObject.optString("modelId");
                    modelName = jsonObject.optString("modelName");
                    String kilometerRangeId = jsonObject.optString("kilometerRangeId");
                    String bookingDate = jsonObject.optString("bookingDate");
                    String bookingTime = jsonObject.optString("bookingTime");
                    String pickupAddress = jsonObject.optString("pickupAddress");
                    pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                    pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                    String pickedUpTime = jsonObject.optString("pickedUpTime");
                    String deliveredTime = jsonObject.optString("deliveredTime");
                    String addressType = jsonObject.optString("addressType");
                    String estimatedCost = jsonObject.optString("estimatedCost");
                    String finalPrice = jsonObject.optString("finalPrice");
                    String inspectionWords = jsonObject.optString("inspectionWords");
                    String promocodeAmount = jsonObject.optString("promocodeAmount");
                    String initialPaidAmount = jsonObject.optString("initialPaidAmount");
                    String paymentStatus = jsonObject.optString("paymentStatus");
                    Boolean movingCondition = jsonObject.optBoolean("movingCondition");

                    if (movingCondition) {
                        moving_txt.setTextColor(ContextCompat.getColor(BookingDetailActivity.this, R.color.green));
                        moving_txt.setText("Vehicle Is In Moving Condition");
                    } else {
                        moving_txt.setTextColor(ContextCompat.getColor(BookingDetailActivity.this, R.color.red));
                        moving_txt.setText("Vehicle Is Not In Moving Condition");
                    }

                    if (paymentStatus.equalsIgnoreCase("2")) {
                        payment_status_txt.setText("Final Payment Done");
                    } else {
                        payment_status_txt.setTextColor(Color.RED);
                        payment_status_txt.setText("Final Payment Pending");
                    }

                    status = jsonObject.optString("status");
                    String deleted = jsonObject.optString("deleted");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");
                    String firstName = jsonObject.optString("firstName");
                    String lastName = jsonObject.optString("lastName");
                    String serviceCenterName = jsonObject.optString("serviceCenterName");
                    String contactMobileNumber = jsonObject.optString("contactMobileNumber");
                    mobile = jsonObject.optString("mobile");
                    String rescheduleCount = jsonObject.optString("rescheduleCount");
                    String paidAmount = jsonObject.optString("paidAmount");
                    String addonsAmount = jsonObject.optString("addonsAmount");
                    String registrationNumber = jsonObject.optString("registrationNumber");
                    reg_txt.setText(registrationNumber);

                    customer_mobile_txt.setText(mobile);

                    if (inspectionWords.isEmpty()) {
                        remarks_rl.setVisibility(View.GONE);
                    } else {
                        remarks_rl.setVisibility(View.VISIBLE);
                        remark_txt.setText(inspectionWords);
                    }

                    if (jsonObject.has("fileIds")) {

                        photos_txt.setVisibility(View.VISIBLE);
                        photo_rl.setVisibility(View.VISIBLE);
                        photos_recyclerview.setVisibility(View.VISIBLE);

                        JSONArray jsonArray = jsonObject.getJSONArray("fileIds");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            String data = jsonArray.get(i).toString();
                            mylist.add(AppUrls.IMAGE_URL + data);
                        }

                        Log.d("PHOTOS", String.valueOf(mylist));

                        photos_recyclerview.setAdapter(photosAdapter);
                        photosAdapter.notifyDataSetChanged();

                    } else {
                        photos_txt.setVisibility(View.GONE);
                        photo_rl.setVisibility(View.GONE);
                        photos_recyclerview.setVisibility(View.GONE);
                    }

                    String services = "", inspections = "";
                    if (jsonObject.has("services")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("services");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            services = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));
                        }

                        service_detail_txt.setText(services);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            serviceIds = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("serviceId");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining(","));
                        }

                    }

                    if (jsonObject.has("inspections")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            inspections = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("inspectionName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));
                        }

                        if (jsonArray.length() > 0) {

                            insp_rl.setVisibility(View.VISIBLE);

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                InspectionModel inspectionModel = new InspectionModel();
                                inspectionModel.setInspectionId(jsonObject1.getString("inspectionId"));
                                inspectionModel.setBookingId(jsonObject1.getString("bookingId"));
                                inspectionModel.setState(jsonObject1.getString("state"));
                                inspectionModel.setInspectionName(jsonObject1.getString("inspectionName"));
                                inspectionModels.add(inspectionModel);
                            }

                            insp_recyclerview.setAdapter(inspAdapter);
                            inspAdapter.notifyDataSetChanged();
                        } else {
                            insp_rl.setVisibility(View.GONE);
                        }

                    }

                    if (jsonObject.has("bookingAddonServices")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("bookingAddonServices");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            addOnServiceIds = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {

                                    if (!jsonArray.getJSONObject(i).optString("status").equalsIgnoreCase("2"))
                                        return jsonArray.getJSONObject(i).optString("serviceId");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                                return "";

                            }).collect(Collectors.joining(","));
                        }

                        if (jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                BookingServicesModel servicesModel = new BookingServicesModel();
                                servicesModel.setServiceId(jsonObject1.getString("serviceId"));
                                servicesModel.setCost(jsonObject1.getString("cost"));
                                servicesModel.setServiceName(jsonObject1.getString("serviceName"));
                                servicesModel.setStatus(jsonObject1.getString("status"));
                                addOnModels.add(servicesModel);
                            }

                            addon_recyclerview.setAdapter(addonAdapter);
                            addonAdapter.notifyDataSetChanged();
                        } else {
                            addon_rl.setVisibility(View.GONE);
                        }

                    }

                    if (jsonObject.has("kilometerRange")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("kilometerRange");
                        startRange = jsonObject1.optString("startRange");
                        endRange = jsonObject1.optString("endRange");

                        kilometers_txt.setText(startRange + " - " + endRange+" km");
                    }

                    if (pickedUpTime.isEmpty()) {
                        pick_date_txt.setVisibility(View.GONE);
                    } else {
                        pick_date_txt.setVisibility(View.VISIBLE);
                        pick_date_txt.setText("Pick Up : " + pickedUpTime);
                    }

                    if (deliveredTime.isEmpty()) {
                        pick_time_txt.setVisibility(View.GONE);
                    } else {
                        pick_time_txt.setVisibility(View.VISIBLE);
                        pick_time_txt.setText("Delivery : " + deliveredTime);
                    }

                    String resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");
                    Log.d("ResultDate", resultDate);

                    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                    Date d = null;
                    try {
                        d = df.parse(bookingTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.HOUR, 5);
                    cal.add(Calendar.MINUTE, 30);
                    String newTime = df.format(cal.getTime());

                    try {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                        Date date = dateFormatter.parse(newTime);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                        //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                        newTime = timeFormatter.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    brand_txt.setText(brandName);
                    model_txt.setText(modelName);
                    date_txt.setText(resultDate);
                    time_txt.setText(newTime);

                    pickup_txt.setText(pickupAddress);

                    if (addonsAmount.equalsIgnoreCase("0")) {
                        addon_tr.setVisibility(View.GONE);
                    } else {
                        addon_tr.setVisibility(View.VISIBLE);
                        addon_price_txt.setText("\u20B9 " + addonsAmount);
                    }
                    if (promocodeAmount.isEmpty() || promocodeAmount.equalsIgnoreCase("0")) {
                        discount_tr.setVisibility(View.GONE);
                    } else {
                        discount_tr.setVisibility(View.VISIBLE);
                        dicount_txt.setText("\u20B9 " + promocodeAmount);
                    }

                    est_price_txt.setText("\u20B9 " + estimatedCost);
                    init_price_txt.setText("\u20B9 " + initialPaidAmount);
                    final_price_txt.setText("\u20B9 " + paidAmount);

                    if (status.equalsIgnoreCase("1")) {
                        assign_btn.setText("Assign Pickup");
                    } else if (status.equalsIgnoreCase("2")) {
                        assign_btn.setText("Pickup");
                    } else if (status.equalsIgnoreCase("3")) {
                        assign_btn.setText("Assign Mechanic");
                    } else if (status.equalsIgnoreCase("4")) {
                        assign_btn.setVisibility(View.GONE);
                        assign_btn.setText("Service Completed");
                        addon_ll.setVisibility(View.VISIBLE);
                    } else if (status.equalsIgnoreCase("5")) {
                        assign_btn.setText("Drop Off");
                    } else if (status.equalsIgnoreCase("6")) {
                        assign_btn.setText("Drop Off");
                        payment_status_txt.setVisibility(View.VISIBLE);
                    } else if (status.equalsIgnoreCase("7")) {
                        assign_btn.setVisibility(View.GONE);
                        payment_status_txt.setVisibility(View.VISIBLE);
                        //assign_btn.setText("Delivered");
                    } else if (status.equalsIgnoreCase("8")) {
                        assign_btn.setText("Deliver");
                        payment_status_txt.setVisibility(View.VISIBLE);
                    } else if (status.equalsIgnoreCase("9")) {
                        assign_btn.setVisibility(View.GONE);
                        addon_ll.setVisibility(View.VISIBLE);
                        completed_btn.setVisibility(View.GONE);
                    } else if (status.equalsIgnoreCase("10")) {
                        assign_btn.setText("Start To PickUp");
                    } else if (status.equalsIgnoreCase("11")) {
                        assign_btn.setText("Booking Reached SST");
                    } else if (status.equalsIgnoreCase("12")) {
                        assign_btn.setText("Cancelled");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                if (activity.equalsIgnoreCase("NewHistoryAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, MainActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("HistoryAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, HistoryActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("DeliveredHistoryAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, DeliveredActivity.class);
                    startActivity(intent);
                } else if (activity.equalsIgnoreCase("NotificationAdapter")) {
                    Intent intent = new Intent(BookingDetailActivity.this, NotificationActivity.class);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(BookingDetailActivity.this, MainActivity.class);
                    startActivity(intent);
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == customer_mobile_txt) {
            if (checkInternet) {

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + Uri.encode(mobile)));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == loc_rl || v == pickup_txt || v == redirect_img) {
            if (checkInternet) {

                String uri = "http://maps.google.com/maps?q=loc:" + pickupAddressLatitude + "," + pickupAddressLongitude + " (" + "Customer" + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == assign_btn) {
            if (checkInternet) {
                if (status.equalsIgnoreCase("1")) {
                    getPickUpData();
                }

                if (status.equalsIgnoreCase("2")) {
                    getOtp();
                }

                if (status.equalsIgnoreCase("3")) {
                    getMechanicData();
                }

                if (status.equalsIgnoreCase("4")) {

                    Intent intent = new Intent(BookingDetailActivity.this, DropOffCheckListActivity.class);
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                    //serviceComplete();
                }

                if (status.equalsIgnoreCase("5")) {

                    dropOffStart();
                }

                if (status.equalsIgnoreCase("8")) {

                    getDeliveryOtp();
                }

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }

        if (v == addon_btn) {

            Intent intent = new Intent(BookingDetailActivity.this, AddonServicesActivity.class);
            intent.putExtra("serviceCenterId", serviceCenterId);
            intent.putExtra("bookingId", bookingId);
            intent.putExtra("userId", userId);
            intent.putExtra("serviceIds", serviceIds + "," + addOnServiceIds);
            intent.putExtra("jsonObj", jsonObject.toString());
            startActivity(intent);
        }

        if (v == completed_btn) {

            Intent intent = new Intent(BookingDetailActivity.this, DropOffCheckListActivity.class);
            intent.putExtra("bookingId", bookingId);
            startActivity(intent);

            //serviceComplete();

        }
    }

    private void getPickUpData() {

        pickUpModels.clear();

        String url = AppUrls.BASE_URL + AppUrls.ASSIGN_PICKUP;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {

                    try {

                        JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            PickUpModel pickUpModel = new PickUpModel();
                            pickUpModel.setUserId(jsonObject.optString("userId"));
                            pickUpModel.setFirstName(jsonObject.optString("firstName"));
                            pickUpModel.setUsername(jsonObject.optString("username"));
                            pickUpModel.setLocked(jsonObject.optString("locked"));
                            pickUpModel.setEnabled(jsonObject.optString("enabled"));
                            pickUpModel.setExpired(jsonObject.optString("expired"));
                            pickUpModel.setAuthority(jsonObject.optString("authority"));
                            pickUpModel.setEmail(jsonObject.optString("email"));
                            pickUpModel.setMobile(jsonObject.optString("mobile"));
                            pickUpModel.setEmailVerified(jsonObject.optString("emailVerified"));
                            pickUpModel.setMobileVerified(jsonObject.optString("mobileVerified"));
                            pickUpModel.setCreatedTime(jsonObject.optString("createdTime"));
                            pickUpModel.setModifiedTime(jsonObject.optString("modifiedTime"));

                            pickUpModels.add(pickUpModel);
                        }

                        pickUp();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {

                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void pickUp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookingDetailActivity.this);
        LayoutInflater inflater = BookingDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.pickup_dialog, null);

        TextView pick_title = dialog_layout.findViewById(R.id.pick_title);
        pick_title.setTypeface(regular);
        pick_title.setText("Pick Boys");

        RecyclerView pick_recyclerview = dialog_layout.findViewById(R.id.pick_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        pick_recyclerview.setLayoutManager(layoutManager);

        pickUpAdapter = new PickUpAdapter(pickUpModels, BookingDetailActivity.this, R.layout.row_pickup);
        pick_recyclerview.setAdapter(pickUpAdapter);

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public void assignPickUp(String id) {

        String url = AppUrls.BASE_URL + "secure/booking/" + bookingId + AppUrls.ASSIGNED_PICKUP + id;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    if (status.equalsIgnoreCase("2000")) {
                        dialog.dismiss();
                        assign_btn.setText("Start PickUp");

                        Intent intent = new Intent(BookingDetailActivity.this, BookingDetailActivity.class);
                        intent.putExtra("activity", "BookingDetailActivity");
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);

                    } else {
                        GlobalCalls.showToast(message, BookingDetailActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void getOtp() {

        String url = AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        String status = jsonObject.optString("status");
                        String code = jsonObject.optString("code");
                        String message = jsonObject.optString("message");

                        if (status.equalsIgnoreCase("2000")) {
                            otpDialog();
                        } else {
                            GlobalCalls.showToast(message, BookingDetailActivity.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {

                    GlobalCalls.showToast(error.getMessage(), BookingDetailActivity.this);
                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void otpDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookingDetailActivity.this);
        builder.setCancelable(false);
        LayoutInflater inflater = BookingDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.otp_dialog, null);

        TextView otp_txt = dialog_layout.findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        otpTextView = dialog_layout.findViewById(R.id.otpTextView);
        TextView resend_txt = dialog_layout.findViewById(R.id.resend_txt);
        resend_txt.setTypeface(regular);
        resend_txt.setPaintFlags(resend_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        /*smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();*/

        resend_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile;

                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        response -> {

                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                String status = jsonObject.optString("status");
                                String code = jsonObject.optString("code");
                                String message = jsonObject.optString("message");

                                otpDialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                        }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer " + accessToken);
                        return headers;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
                requestQueue.add(stringRequest);
            }
        });
        Button verify_btn = dialog_layout.findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otp = otpTextView.getOTP();
                String url = AppUrls.BASE_URL + AppUrls.PICKUP_VERIFY_OTP + otp + "&bookingId=" + bookingId;
                Log.d("VERIFY", url);
                String otp = otpTextView.getOTP();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    String code = jsonObject.optString("code");
                                    String message = jsonObject.optString("message");

                                    Intent intent = new Intent(BookingDetailActivity.this, PickUpValidateActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.getMessage();
                                otpTextView.showError();
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer " + accessToken);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
                requestQueue.add(stringRequest);

            }
        });

        builder.setView(dialog_layout).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    private void getMechanicData() {

        mechanicModels.clear();

        String url = AppUrls.BASE_URL + AppUrls.SEND_MECHANIC;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {

                    try {

                        JSONArray jsonArray = new JSONArray(response);

                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            PickUpModel pickUpModel = new PickUpModel();
                            pickUpModel.setUserId(jsonObject.optString("userId"));
                            pickUpModel.setFirstName(jsonObject.optString("firstName"));
                            pickUpModel.setUsername(jsonObject.optString("username"));
                            pickUpModel.setLocked(jsonObject.optString("locked"));
                            pickUpModel.setEnabled(jsonObject.optString("enabled"));
                            pickUpModel.setExpired(jsonObject.optString("expired"));
                            pickUpModel.setAuthority(jsonObject.optString("authority"));
                            pickUpModel.setEmail(jsonObject.optString("email"));
                            pickUpModel.setMobile(jsonObject.optString("mobile"));
                            pickUpModel.setEmailVerified(jsonObject.optString("emailVerified"));
                            pickUpModel.setMobileVerified(jsonObject.optString("mobileVerified"));
                            pickUpModel.setCreatedTime(jsonObject.optString("createdTime"));
                            pickUpModel.setModifiedTime(jsonObject.optString("modifiedTime"));

                            mechanicModels.add(pickUpModel);
                        }

                        mechanicAssign();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {

                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void mechanicAssign() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookingDetailActivity.this);
        LayoutInflater inflater = BookingDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.pickup_dialog, null);

        TextView pick_title = dialog_layout.findViewById(R.id.pick_title);
        pick_title.setTypeface(regular);
        pick_title.setText("Assign Mechanic");

        RecyclerView pick_recyclerview = dialog_layout.findViewById(R.id.pick_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        pick_recyclerview.setLayoutManager(layoutManager);

        mechanicAdapter = new MechanicAdapter(mechanicModels, BookingDetailActivity.this, R.layout.row_pickup);
        pick_recyclerview.setAdapter(mechanicAdapter);

        builder.setView(dialog_layout).setNegativeButton("CANCEL", (dialogInterface, i) -> {

            dialogInterface.dismiss();
        });

        dialog = builder.create();
        dialog.setCancelable(false);
        dialog.show();
    }

    public void assignMechanic(String id) {

        String url = AppUrls.BASE_URL + "secure/booking/" + bookingId + AppUrls.ASSIGN_MECHANIC + id;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    if (status.equalsIgnoreCase("2000")) {
                        dialog.dismiss();
                        assign_btn.setVisibility(View.GONE);

                        Intent intent = new Intent(BookingDetailActivity.this, BookingDetailActivity.class);
                        intent.putExtra("activity", "BookingDetailActivity");
                        intent.putExtra("bookingId", bookingId);
                        startActivity(intent);

                    } else {
                        GlobalCalls.showToast(message, BookingDetailActivity.this);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    public void serviceComplete() {
        String url = AppUrls.BASE_URL + AppUrls.SERVICE_COMPLETED;

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    String code = jsonObj.getString("code");

                    GlobalCalls.showToast(message, BookingDetailActivity.this);

                    Intent intent = new Intent(BookingDetailActivity.this, DropOffCheckListActivity.class);
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, error -> error.printStackTrace()) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(request);
    }

    private void dropOffStart() {

        String url = AppUrls.BASE_URL + AppUrls.DROP_OFF + bookingId + "/drop-off";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String status = jsonObject.optString("status");
                    String code = jsonObject.optString("code");
                    String message = jsonObject.optString("message");

                    GlobalCalls.showToast(message, BookingDetailActivity.this);

                    Intent intent = new Intent(BookingDetailActivity.this, BookingDetailActivity.class);
                    intent.putExtra("activity", "BookingDetailActivity");
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);


    }

    private void getDeliveryOtp() {

        String url = AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {

                    try {

                        JSONObject jsonObject = new JSONObject(response);

                        String status = jsonObject.optString("status");
                        String code = jsonObject.optString("code");
                        String message = jsonObject.optString("message");

                        if (status.equalsIgnoreCase("2000")) {
                            deliveryOtpDialog();
                        } else {
                            GlobalCalls.showToast(message, BookingDetailActivity.this);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                error -> {

                }) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
        requestQueue.add(stringRequest);
    }

    private void deliveryOtpDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(BookingDetailActivity.this);
        builder.setCancelable(false);
        LayoutInflater inflater = BookingDetailActivity.this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.delivery_otp_dialog, null);

        TextView otp_txt = dialog_layout.findViewById(R.id.otp_txt);
        otp_txt.setTypeface(bold);
        otpTextView = dialog_layout.findViewById(R.id.otpTextView);
        TextView resend_txt = dialog_layout.findViewById(R.id.resend_txt);
        resend_txt.setTypeface(regular);
        resend_txt.setPaintFlags(resend_txt.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        /*smsVerifyCatcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
            @Override
            public void onSmsCatch(String message) {
                otp = parseCode(message);
                otpTextView.setOTP(otp);
            }
        });

        smsVerifyCatcher.onStart();*/

        resend_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = AppUrls.BASE_URL + AppUrls.PICKUP_OTP + bookingId + "&mobile=" + mobile;
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        response -> {

                            try {

                                JSONObject jsonObject = new JSONObject(response);

                                String status = jsonObject.optString("status");
                                String code = jsonObject.optString("code");
                                String message = jsonObject.optString("message");

                                otpDialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                        }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer " + accessToken);
                        return headers;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
                requestQueue.add(stringRequest);
            }
        });
        Button verify_btn = dialog_layout.findViewById(R.id.verify_btn);
        verify_btn.setTypeface(bold);
        verify_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otp = otpTextView.getOTP();
                //String url = AppUrls.BASE_URL + AppUrls.PICKUP_VERIFY_OTP + mobile + "&otp=" + otp;
                String url = AppUrls.BASE_URL + AppUrls.PICKUP_VERIFY_OTP + otp + "&bookingId=" + bookingId;
                Log.d("VERIFY", url);
                String otp = otpTextView.getOTP();

                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String status = jsonObject.optString("status");
                                    String code = jsonObject.optString("code");
                                    String message = jsonObject.optString("message");

                                    Intent intent = new Intent(BookingDetailActivity.this, DropOffInspectionsActivity.class);
                                    intent.putExtra("bookingId", bookingId);
                                    startActivity(intent);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.getMessage();
                                otpTextView.showError();
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Authorization", "Bearer " + accessToken);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(BookingDetailActivity.this);
                requestQueue.add(stringRequest);

            }
        });

        builder.setView(dialog_layout).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBackPressed() {

        if (activity.equalsIgnoreCase("NewHistoryAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("HistoryAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, HistoryActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("DeliveredHistoryAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, DeliveredActivity.class);
            startActivity(intent);
        } else if (activity.equalsIgnoreCase("NotificationAdapter")) {
            Intent intent = new Intent(BookingDetailActivity.this, NotificationActivity.class);
            startActivity(intent);
        }else {
            Intent intent = new Intent(BookingDetailActivity.this, MainActivity.class);
            startActivity(intent);
        }

    }


    @Override
    public void onRefresh() {

        swipe.setRefreshing(false);

        Intent intent = new Intent(BookingDetailActivity.this, BookingDetailActivity.class);
        intent.putExtra("activity", "BookingDetailActivity");
        intent.putExtra("bookingId", bookingId);
        startActivity(intent);

    }
}
