package in.gaadi360.partner.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import in.gaadi360.partner.R;
import in.gaadi360.partner.adapters.DropOffCheckListAdapter;
import in.gaadi360.partner.models.DropOffServicesModel;
import in.gaadi360.partner.models.ServiceDetailModel;
import in.gaadi360.partner.models.ServicesModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.GlobalCalls;
import in.gaadi360.partner.utils.UserSessionManager;

import static in.gaadi360.partner.models.ServicesModel.CHILD_TYPE;
import static in.gaadi360.partner.models.ServicesModel.HEADER_TYPE;

public class DropOffCheckListActivity extends AppCompatActivity implements View.OnClickListener {

    UserSessionManager userSessionManager;
    String accessToken, bookingId;
    JSONObject jsonObject;
    ImageView close;
    TextView toolbar_title, booking_txt;
    Button drop_off_btn;
    Typeface regular, bold;

    RecyclerView drop_recyclerview;
    DropOffCheckListAdapter dropOffCheckListAdapter;
    ArrayList<DropOffServicesModel> dropOffServicesModels = new ArrayList<DropOffServicesModel>();
    ArrayList<ServiceDetailModel> serviceDetailModels = new ArrayList<ServiceDetailModel>();

    List<ServicesModel> servicesModels = new ArrayList<ServicesModel>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_off_check_list);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        bookingId = getIntent().getStringExtra("bookingId");

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);
        booking_txt = findViewById(R.id.booking_txt);
        booking_txt.setTypeface(bold);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);
        drop_off_btn = findViewById(R.id.drop_off_btn);
        drop_off_btn.setTypeface(bold);
        drop_off_btn.setOnClickListener(this);

        drop_recyclerview = findViewById(R.id.drop_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        drop_recyclerview.setLayoutManager(layoutManager);
        dropOffCheckListAdapter = new DropOffCheckListAdapter(servicesModels, DropOffCheckListActivity.this);

        getDetail();
    }

    private void getDetail() {

        String url = AppUrls.BASE_URL + AppUrls.HISTORY_DETAIL + bookingId + "/detail";

        Log.d("URL", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                Log.d("RESP", response);

                try {
                    jsonObject = new JSONObject(response);

                    String bookingId = jsonObject.optString("bookingId");
                    booking_txt.setText("Booking Id : " + bookingId);
                    String userId = jsonObject.optString("userId");
                    String orderId = jsonObject.optString("orderId");
                    String serviceCenterId = jsonObject.optString("serviceCenterId");
                    String bookingType = jsonObject.optString("bookingType");
                    String brandId = jsonObject.optString("brandId");
                    String brandName = jsonObject.optString("brandName");
                    String modelId = jsonObject.optString("modelId");
                    String modelName = jsonObject.optString("modelName");
                    String kilometerRangeId = jsonObject.optString("kilometerRangeId");
                    String bookingDate = jsonObject.optString("bookingDate");
                    String bookingTime = jsonObject.optString("bookingTime");
                    String pickupAddress = jsonObject.optString("pickupAddress");
                    String pickupAddressLatitude = jsonObject.optString("pickupAddressLatitude");
                    String pickupAddressLongitude = jsonObject.optString("pickupAddressLongitude");
                    String pickedUpTime = jsonObject.optString("pickedUpTime");
                    String deliveredTime = jsonObject.optString("deliveredTime");
                    String addressType = jsonObject.optString("addressType");
                    String estimatedCost = jsonObject.optString("estimatedCost");
                    String finalPrice = jsonObject.optString("finalPrice");
                    String promocodeAmount = jsonObject.optString("promocodeAmount");
                    String initialPaidAmount = jsonObject.optString("initialPaidAmount");
                    String paymentStatus = jsonObject.optString("paymentStatus");
                    String status = jsonObject.optString("status");
                    String deleted = jsonObject.optString("deleted");
                    String createdTime = jsonObject.optString("createdTime");
                    String modifiedTime = jsonObject.optString("modifiedTime");
                    String firstName = jsonObject.optString("firstName");
                    String lastName = jsonObject.optString("lastName");
                    String serviceCenterName = jsonObject.optString("serviceCenterName");
                    String contactMobileNumber = jsonObject.optString("contactMobileNumber");
                    String mobile = jsonObject.optString("mobile");
                    String rescheduleCount = jsonObject.optString("rescheduleCount");
                    String paidAmount = jsonObject.optString("paidAmount");
                    String addonsAmount = jsonObject.optString("addonsAmount");

                    String services = "", inspections = "";

                    if (jsonObject.has("inspections")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("inspections");

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            inspections = IntStream.range(0, jsonArray.length()).mapToObj(i -> {
                                try {
                                    return jsonArray.getJSONObject(i).optString("inspectionName");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    return "";
                                }
                            }).collect(Collectors.joining("\n"));
                        }

                    }

                    if (jsonObject.has("services")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("services");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            DropOffServicesModel dropOffServicesModel = new DropOffServicesModel();
                            dropOffServicesModel.setServiceId(jsonObject1.optString("serviceId"));
                            dropOffServicesModel.setCost(jsonObject1.optString("cost"));
                            dropOffServicesModel.setServiceName(jsonObject1.optString("serviceName"));
                            dropOffServicesModel.setCreatedTime(jsonObject1.optString("createdTime"));
                            dropOffServicesModel.setModifiedTime(jsonObject1.optString("modifiedTime"));
                            dropOffServicesModel.setBookingId(jsonObject1.optString("bookingId"));
                            dropOffServicesModel.setType(jsonObject1.optString("type"));

                            servicesModels.add(new ServicesModel(
                                    jsonObject1.optString("serviceId"),
                                    jsonObject1.optString("serviceName"),
                                    jsonObject1.optString("createdTime"),
                                    jsonObject1.optString("modifiedTime"),
                                    jsonObject1.optString("bookingId"),
                                    jsonObject1.optString("cost"),
                                    jsonObject1.optString("type"),
                                    "",
                                    "",
                                    "0",
                                    HEADER_TYPE));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("serviceDetails");

                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                ServiceDetailModel sdm = new ServiceDetailModel();
                                sdm.setServiceId(jsonObject2.optString("serviceId"));
                                sdm.setPerform(jsonObject2.optString("perform"));
                                sdm.setServiceItem(jsonObject2.optString("serviceItem"));

                                Log.d("ServiceDetails", jsonObject2.optString("serviceItem"));
                                serviceDetailModels.add(sdm);

                                servicesModels.add(new ServicesModel(
                                        jsonObject1.optString("serviceId"),
                                        jsonObject1.optString("serviceName"),
                                        jsonObject1.optString("createdTime"),
                                        jsonObject1.optString("modifiedTime"),
                                        jsonObject1.optString("bookingId"),
                                        jsonObject1.optString("cost"),
                                        jsonObject1.optString("type"),
                                        jsonObject2.optString("perform"),
                                        jsonObject2.optString("serviceItem"),
                                        "0",
                                        CHILD_TYPE));

                            }
                            dropOffServicesModel.setServiceDetailModels(serviceDetailModels);
                            dropOffServicesModels.add(dropOffServicesModel);

                        }

                        drop_recyclerview.setAdapter(dropOffCheckListAdapter);
                        dropOffCheckListAdapter.notifyDataSetChanged();

                    }

                    if (jsonObject.has("bookingAddonServices")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("bookingAddonServices");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            DropOffServicesModel dropOffServicesModel = new DropOffServicesModel();
                            dropOffServicesModel.setServiceId(jsonObject1.optString("serviceId"));
                            dropOffServicesModel.setCost(jsonObject1.optString("cost"));
                            dropOffServicesModel.setServiceName(jsonObject1.optString("serviceName"));
                            dropOffServicesModel.setCreatedTime(jsonObject1.optString("createdTime"));
                            dropOffServicesModel.setModifiedTime(jsonObject1.optString("modifiedTime"));
                            dropOffServicesModel.setBookingId(jsonObject1.optString("bookingId"));
                            dropOffServicesModel.setType(jsonObject1.optString("type"));

                            servicesModels.add(new ServicesModel(
                                    jsonObject1.optString("serviceId"),
                                    jsonObject1.optString("serviceName"),
                                    jsonObject1.optString("createdTime"),
                                    jsonObject1.optString("modifiedTime"),
                                    jsonObject1.optString("bookingId"),
                                    jsonObject1.optString("cost"),
                                    jsonObject1.optString("type"),
                                    "",
                                    "",
                                    "0",
                                    HEADER_TYPE));

                            JSONArray jsonArray1 = jsonObject1.getJSONArray("serviceDetails");

                            for (int j = 0; j < jsonArray1.length(); j++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(j);
                                ServiceDetailModel sdm = new ServiceDetailModel();
                                sdm.setServiceId(jsonObject2.optString("serviceId"));
                                sdm.setPerform(jsonObject2.optString("perform"));
                                sdm.setServiceItem(jsonObject2.optString("serviceItem"));

                                Log.d("ServiceDetails", jsonObject2.optString("serviceItem"));
                                serviceDetailModels.add(sdm);

                                servicesModels.add(new ServicesModel(
                                        jsonObject1.optString("serviceId"),
                                        jsonObject1.optString("serviceName"),
                                        jsonObject1.optString("createdTime"),
                                        jsonObject1.optString("modifiedTime"),
                                        jsonObject1.optString("bookingId"),
                                        jsonObject1.optString("cost"),
                                        jsonObject1.optString("type"),
                                        jsonObject2.optString("perform"),
                                        jsonObject2.optString("serviceItem"),
                                        "0",
                                        CHILD_TYPE));

                            }
                            dropOffServicesModel.setServiceDetailModels(serviceDetailModels);
                            dropOffServicesModels.add(dropOffServicesModel);

                        }
                    }

                    if (jsonObject.has("kilometerRange")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("kilometerRange");
                        String startRange = jsonObject1.optString("startRange");
                        String endRange = jsonObject1.optString("endRange");
                    }

                    String resultDate = convertStringDateToAnotherStringDate(bookingDate, "yyyy-MM-dd", "dd-MM-yyyy");
                    Log.d("ResultDate", resultDate);

                    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                    Date d = null;
                    try {
                        d = df.parse(bookingTime);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(d);
                    cal.add(Calendar.HOUR, 5);
                    cal.add(Calendar.MINUTE, 30);
                    String newTime = df.format(cal.getTime());

                    try {
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("HH:mm:ss");
                        Date date = dateFormatter.parse(newTime);
                        SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm a");
                        //SimpleDateFormat timeFormatter = new SimpleDateFormat("h a");
                        newTime = timeFormatter.format(date);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                error -> {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                    } else if (error instanceof AuthFailureError) {

                    } else if (error instanceof ServerError) {

                    } else if (error instanceof NetworkError) {

                    } else if (error instanceof ParseError) {

                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(DropOffCheckListActivity.this);
        requestQueue.add(stringRequest);
    }

    public String convertStringDateToAnotherStringDate(String stringdate, String stringdateformat, String returndateformat) {

        try {
            Date date = new SimpleDateFormat(stringdateformat).parse(stringdate);
            String returndate = new SimpleDateFormat(returndateformat).format(date);
            return returndate;
        } catch (ParseException e) {
            e.printStackTrace();
            return stringdate;
        }

    }

    @Override
    public void onClick(View v) {

        if (v == close) {
            Intent intent = new Intent(DropOffCheckListActivity.this, BookingDetailActivity.class);
            intent.putExtra("activity", "DropOffCheckListActivity");
            intent.putExtra("bookingId", bookingId);
            startActivity(intent);
        }

        if (v == drop_off_btn) {

            Log.d("CHCHCH", String.valueOf(jsonObject));

            //serviceComplete();

            boolean status = false;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {

                status = servicesModels.stream().anyMatch(t -> !t.getServiceItem().isEmpty() && t.getStatus().equalsIgnoreCase("0"));

            }
            if (!status) {

                serviceComplete();

            } else {

                GlobalCalls.showToast("Please Select All Services..!", DropOffCheckListActivity.this);
            }
        }
    }

    public void serviceComplete() {

        String url = AppUrls.BASE_URL + AppUrls.SERVICE_COMPLETED;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bookingId", bookingId);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                servicesModels = servicesModels.stream().sorted((t1, t2) -> t1.getServiceId().compareTo(t2.getServiceId())).collect(Collectors.toList());
            }

            String serviceId = "";
            JSONArray jsonArray = new JSONArray();
            JSONArray jsonArray1 = null;
            JSONObject jsonObject1 = null;

            for (int i = 0; i < servicesModels.size(); i++) {

                if (!serviceId.equalsIgnoreCase(servicesModels.get(i).getServiceId())) {
                    serviceId = servicesModels.get(i).getServiceId();
                    if (jsonObject1 != null && jsonArray1 != null) {
                        jsonObject1.put("serviceDetails", jsonArray1);
                        jsonArray.put(jsonObject1);
                    }
                    jsonObject1 = new JSONObject();
                    jsonObject1.put("serviceId", servicesModels.get(i).getServiceId());
                    jsonObject1.put("serviceName", servicesModels.get(i).getServiceName());
                    jsonObject1.put("createdTime", servicesModels.get(i).getCreatedTime());
                    jsonObject1.put("modifiedTime", servicesModels.get(i).getModifiedTime());
                    jsonObject1.put("bookingId", servicesModels.get(i).getBookingId());
                    jsonObject1.put("cost", servicesModels.get(i).getCost());
                    jsonObject1.put("type", servicesModels.get(i).getType());
                    jsonArray1 = new JSONArray();
                } else {

                    JSONObject jsonObject2 = new JSONObject();
                    jsonObject2.put("serviceId", servicesModels.get(i).getServiceId());
                    jsonObject2.put("perform", servicesModels.get(i).getPerform());
                    jsonObject2.put("serviceItem", servicesModels.get(i).getServiceItem());
                    jsonObject2.put("status", servicesModels.get(i).getStatus());

                    jsonArray1.put(jsonObject2);

                }
            }

            if (jsonObject1 != null && jsonArray1 != null) {
                jsonObject1.put("serviceDetails", jsonArray1);
                jsonArray.put(jsonObject1);
            }

            jsonObject.put("services", jsonArray);

            Log.d("ARRAYDATA", "" + jsonArray);
            Log.d("JAYYYY", "" + jsonObject);


        } catch (Exception e) {
            e.printStackTrace();
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d("SuccessResp", response.toString());

                try {
                    JSONObject jsonObj = new JSONObject(response.toString());
                    String status = jsonObj.getString("status");
                    String message = jsonObj.getString("message");
                    String code = jsonObj.getString("code");

                    GlobalCalls.showToast(message, DropOffCheckListActivity.this);

                    Intent intent = new Intent(DropOffCheckListActivity.this, BookingDetailActivity.class);
                    intent.putExtra("activity", "DropOffCheckListActivity");
                    intent.putExtra("bookingId", bookingId);
                    startActivity(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, error -> error.printStackTrace()) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(DropOffCheckListActivity.this);
        requestQueue.add(request);
    }
}
