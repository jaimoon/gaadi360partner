package in.gaadi360.partner.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.eightbitlab.bottomnavigationbar.BottomBarItem;
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import in.gaadi360.partner.MainActivity;
import in.gaadi360.partner.R;
import in.gaadi360.partner.adapters.HistoryAdapter;
import in.gaadi360.partner.models.HistoryModel;
import in.gaadi360.partner.utils.AppUrls;
import in.gaadi360.partner.utils.NetworkChecking;
import in.gaadi360.partner.utils.UserSessionManager;

public class HistoryActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView close, no_history_img;
    TextView toolbar_title, no_history_txt;
    private boolean checkInternet;
    RecyclerView history_recyclerview;

    UserSessionManager userSessionManager;
    String accessToken;
    SearchView history_search;

    /*History*/
    HistoryAdapter historyAdapter;
    ArrayList<HistoryModel> historyModels = new ArrayList<HistoryModel>();
    Typeface regular, bold;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        regular = Typeface.createFromAsset(getAssets(), "proxima_nova_regular.otf");
        bold = Typeface.createFromAsset(getAssets(), "proxima_nova_bold.otf");

        checkInternet = NetworkChecking.isConnected(this);

        userSessionManager = new UserSessionManager(this);
        HashMap<String, String> userDetails = userSessionManager.getUserDetails();
        accessToken = userDetails.get(UserSessionManager.KEY_ACCSES);

        setupBottomBar();

        toolbar_title = findViewById(R.id.toolbar_title);
        toolbar_title.setTypeface(bold);

        no_history_img = findViewById(R.id.no_history_img);
        no_history_txt = findViewById(R.id.no_history_txt);
        close = findViewById(R.id.close);
        close.setOnClickListener(this);

        history_recyclerview = findViewById(R.id.history_recyclerview);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        history_recyclerview.setLayoutManager(layoutManager);
        historyAdapter = new HistoryAdapter(historyModels, HistoryActivity.this, R.layout.row_history);

        getHistory();

        history_search = findViewById(R.id.history_search);
        EditText searchEditText = history_search.findViewById(androidx.appcompat.R.id.search_src_text);
        history_search.setOnClickListener(v -> history_search.setIconified(false));
        history_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {

                historyAdapter.getFilter().filter(query);
                return false;
            }
        });
    }

    private void getHistory() {

        //String url = AppUrls.BASE_URL + AppUrls.HISTORY + "2";
        String url = AppUrls.BASE_URL + AppUrls.HISTORY + "5";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                historyModels.clear();

                try {

                    JSONArray jsonArray = new JSONArray(response);


                    if (jsonArray.length() != 0) {

                        history_search.setVisibility(View.VISIBLE);
                        no_history_img.setVisibility(View.GONE);
                        no_history_txt.setVisibility(View.GONE);

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            HistoryModel rm = new HistoryModel();
                            rm.setBookingId(jsonObject1.optString("bookingId"));
                            rm.setBrandId(jsonObject1.optString("brandId"));
                            rm.setBrandName(jsonObject1.optString("brandName"));
                            rm.setModelId(jsonObject1.optString("modelId"));
                            rm.setModelName(jsonObject1.optString("modelName"));
                            rm.setRegistrationNumber(jsonObject1.optString("registrationNumber"));
                            rm.setBookingDate(jsonObject1.optString("bookingDate"));
                            rm.setBookingTime(jsonObject1.optString("bookingTime"));
                            rm.setFinalPrice(jsonObject1.optString("finalPrice"));
                            rm.setPickupAddress(jsonObject1.optString("pickupAddress"));
                            rm.setStatus(jsonObject1.optString("status"));
                            rm.setServiceCenterName(jsonObject1.optString("serviceCenterName"));
                            rm.setPaymentStatus(jsonObject1.optString("paymentStatus"));
                            rm.setPromocodeAmount(jsonObject1.optString("promocodeAmount"));
                            rm.setMovingCondition(jsonObject1.optBoolean("movingCondition"));

                            String services = "";
                            if (jsonObject1.has("services")) {
                                JSONArray servicesArray = jsonObject1.getJSONArray("services");

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    services = IntStream.range(0, servicesArray.length()).mapToObj(j -> {
                                        try {
                                            return servicesArray.getJSONObject(j).getString("serviceName");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            return "";
                                        }
                                    }).collect(Collectors.joining("\n"));
                                }

                                rm.setServices(services);
                            }

                            historyModels.add(rm);
                        }

                        history_recyclerview.setAdapter(historyAdapter);
                        historyAdapter.notifyDataSetChanged();
                    } else {
                        history_search.setVisibility(View.GONE);
                        no_history_txt.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        } else if (error instanceof AuthFailureError) {

                        } else if (error instanceof ServerError) {

                        } else if (error instanceof NetworkError) {

                        } else if (error instanceof ParseError) {

                        }
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Bearer " + accessToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(HistoryActivity.this);
        requestQueue.add(stringRequest);

    }

    @Override
    public void onClick(View v) {
        if (v == close) {
            if (checkInternet) {
                Intent intent = new Intent(HistoryActivity.this, MainActivity.class);
                startActivity(intent);

            } else {
                Snackbar snackbar = Snackbar.make(getWindow().getDecorView(), "Check Internet Connection", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        }
    }

    private void setupBottomBar() {
        BottomNavigationBar bottomNavigationBar = findViewById(R.id.bottom_bar);

        BottomBarItem home = new BottomBarItem(R.drawable.ic_house_black_silhouette_without_door, R.string.newHistory);
        BottomBarItem history = new BottomBarItem(R.drawable.ic_diploma, R.string.inProcessHistory);
        BottomBarItem settings = new BottomBarItem(R.drawable.ic_diploma, R.string.deliveredHistory);

        bottomNavigationBar
                .addTab(home)
                .addTab(history)
                .addTab(settings);

        bottomNavigationBar.selectTab(1,true);

        bottomNavigationBar.setOnSelectListener(new BottomNavigationBar.OnSelectListener() {
            @Override
            public void onSelect(int position) {
                showContent(position);
            }
        });
    }

    void showContent(int position) {

        if (position == 0) {
            Intent intent = new Intent(HistoryActivity.this, MainActivity.class);
            startActivity(intent);
        }

        if (position == 2) {
            Intent intent = new Intent(HistoryActivity.this, DeliveredActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HistoryActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
